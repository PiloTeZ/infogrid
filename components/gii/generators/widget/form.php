<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator yii\gii\generators\module\Generator */
?>

<?= $form->field($generator, 'textId'); ?>
<?= $form->field($generator, 'namespace'); ?>
<?= $form->field($generator, 'useJs')->checkbox(); ?>
<?= $form->field($generator, 'useAngularJsComponents')->checkbox(); ?>
<?= $form->field($generator, 'useAngularJsController')->checkbox(); ?>
<?= $form->field($generator, 'useStyles')->checkbox(); ?>
