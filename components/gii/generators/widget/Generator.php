<?php
namespace app\components\gii\generators\widget;

use Yii;
use yii\gii\CodeFile;
use yii\helpers\Inflector;

class Generator extends \yii\gii\Generator
{
    public $textId;
    public $namespace = 'app\modules\\...';
    public $useJs;
    public $useAngularJsComponents;
    public $useAngularJsController;
    public $useStyles;

    public function init()
    {
        parent::init();

        if ($this->useAngularJsController) {
            $this->useAngularJsComponents;
        }
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['textId', 'namespace'], 'required'],
            [['textId', 'namespace'], 'string'],
            [['useJs', 'useAngularJsComponents', 'useAngularJsController', 'useStyles'], 'boolean'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'textId' => 'Текстовый ID',
            'namespace' => 'Namespace',
            'useJs' => 'Использовать Javascript',
            'useAngularJsComponents' => 'Использовать AngularJs компоненты',
            'useAngularJsController' => 'Использовать AngularJs контроллер',
            'useStyles' => 'Использовать стили',
        ]);
    }

    public function hints()
    {
        return array_merge(parent::attributeLabels(), [
            'textId' => 'Например news-feed',
            'namespace' => 'Без имени папки и класса. Например app\modules\news\widgets',
            'useJs' => 'Подключать ресурс js/functions.js',
            'useAngularJsComponents' => 'Подключать ресурс js/angular.js',
            'useAngularJsController' => 'Подключать русурс js/angular.js и оборачивать контент виджета в контроллер',
            'useStyles' => 'Подключать ресурс css/styles.css',
        ]);
    }

    public function getName()
    {
        return 'Widget Generator';
    }

    public function getDescription()
    {
        return '';
    }

    public function generate()
    {
        $textId = $this->textId;
        $textIdCamel = Inflector::id2camel($textId);
        $textIdVariable = Inflector::variablize($textId);
        $widgetClassName = $textIdCamel . 'Widget';
        $widgetNamespace = $this->namespace . '\\' . $textIdVariable;
        $widgetFullNamespace = $widgetNamespace . '\\';
        $widgetYiiPath = '@' . str_replace('\\', '/', $widgetFullNamespace);
        $widgetPath = Yii::getAlias($widgetYiiPath);
        $widgetDirName = $textIdVariable;

        $files = [];
        $params = [
            'textId' => $textId,
            'textIdCamel' => $textIdCamel,
            'textIdVariable' => $textIdVariable,
            'widgetPath' => $widgetPath,
            'widgetDirName' => $widgetDirName,
            'widgetClassName' => $widgetClassName,
            'widgetNamespace' => $widgetNamespace,
            'useJs' => $this->useJs,
            'useAngularJsComponents' => $this->useAngularJsComponents,
            'useAngularJsController' => $this->useAngularJsController,
            'useStyles' => $this->useStyles,
        ];

        // Главный класс виджета
        $files[] = new CodeFile($widgetPath . $widgetClassName . '.php', $this->render('widget/Widget.php', $params));

        // Представление
        $files[] = new CodeFile($widgetPath . 'views/' . $textId . '.php', $this->render('widget/views/view.php', $params));

        // Ресурсы
        if ($this->useStyles)
            $files[] = new CodeFile($widgetPath . 'assets/css/styles.css', $this->render('widget/assets/css/styles.php', $params));

        if ($this->useJs)
            $files[] = new CodeFile($widgetPath . 'assets/js/functions.js', $this->render('widget/assets/js/functions.php', $params));

        if ($this->useAngularJsComponents || $this->useAngularJsController)
            $files[] = new CodeFile($widgetPath . 'assets/js/angular.js', $this->render('widget/assets/js/angular.php', $params));

        return $files;

        /*
        $widgetsPath = $this->getWidgetsPath() . '/';
        $widgetsApiPath = $this->getWidgetsApiPath() . '/';

        $widgetId = $this->textId;
        $widgetCamelId = Inflector::id2camel($this->textId);

        $widgetPath = $widgetsPath . $widgetId . '/';
        $widgetAssetPath = $widgetsPath . $widgetId . '/assets/';
        $apiControllerPath = $widgetsApiPath . 'controllers/' . $widgetCamelId . 'WidgetController.php';
        $apiManagerPath = $widgetPath . 'components/' . $widgetCamelId . 'Manager.php';
*/
        // $files = [];
        /*   $viewParams = [
               'widgetId' => $widgetId,
               'widgetCamelId' => $widgetCamelId,
               'styles' => $this->javascript,
               'javascript' => $this->javascript,
               'api' => $this->api,
           ];

           // Виджет
           $files[] = new CodeFile($widgetPath . $widgetCamelId . '.php', $this->render('widget/Widget.php', $viewParams));

           // Представление виджета
           $files[] = new CodeFile($widgetPath . 'views/widget.php', $this->render('widget/views/widget.php', $viewParams));

           if ($this->styles || $this->javascript) {
               $files[] = new CodeFile($widgetAssetPath . $widgetCamelId . 'Asset.php', $this->render('widget/assets/WidgetAsset.php', $viewParams));
           }

           if ($this->javascript) {
               $files[] = new CodeFile($widgetAssetPath . $widgetCamelId . 'AssetDepends.php', $this->render('widget/assets/WidgetAssetDepends.php', $viewParams));
           }

           // Стили
           if ($this->styles) {
               $files[] = new CodeFile($widgetAssetPath . 'css/styles.css', $this->render('widget/assets/css/styles.css.php', $viewParams));
           }

           // JS
           if ($this->javascript) {
               $files[] = new CodeFile($widgetAssetPath . 'js/scripts.js', $this->render('widget/assets/js/scripts.js', $viewParams));
               $files[] = new CodeFile($widgetAssetPath . 'js/functions.js', $this->render('widget/assets/js/functions.js', $viewParams));
           }

           // Настройки виджета
           if ($this->settings) {
               // Модель конфига
               $files[] = new CodeFile($widgetPath . $widgetCamelId . 'Config.php', $this->render('widget/WidgetConfig.php', $viewParams));
               // Виджет настроек
               $files[] = new CodeFile($widgetPath . $widgetCamelId . 'Settings.php', $this->render('widget/WidgetSettings.php', $viewParams));
               // Представление виджета настроек
               $files[] = new CodeFile($widgetPath . 'views/settings.php', $this->render('widget/views/settings.php', $viewParams));
           }

           // API-контроллер
           if ($this->api) {
               $files[] = new CodeFile($apiControllerPath, $this->render('api/controllers/WidgetController.php', $viewParams));
               $files[] = new CodeFile($apiManagerPath, $this->render('widget/components/WidgetManager.php', $viewParams));
           }
   */
    }
}