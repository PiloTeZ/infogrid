<?= '<?php' ?>

namespace <?= $widgetNamespace ?>;

use app\widgets\base\AbstractWidget;

class <?= $widgetClassName ?> extends AbstractWidget
{
<? if ($useJs) { ?>
    /** @var bool Подключить ресурсы js/functions.js */
    public $widgetUseJs = <?= ($useJs ? 'true' : 'false') ?>;
<? } ?>
<? if ($useAngularJsComponents) { ?>
    /** @var bool Подключить ресурс js/angular.js */
    public $widgetUseAngularJsComponents = <?= ($useAngularJsComponents ? 'true' : 'false') ?>;
<? } ?>
<? if ($useAngularJsController) { ?>
    /** @var bool Обернуть контент виджета в контроллер angularJs (<?=$widgetClassName ?>Controller) **/
    public $widgetUseAngularJsController = <?= ($useAngularJsController ? 'true' : 'false') ?>;
<? } ?>
<? if ($useStyles) { ?>
    /** @var bool Подключить ресурс css/styles.css */
    public $widgetUseStyles = <?= ($useStyles ? 'true' : 'false') ?>;
<? } ?>

    public function run()
    {
        $paramValue = null;
<? if ($useJs || $useAngularJsComponents || $useAngularJsController) { ?>
        $jsParamValue = null;
<? } ?>

        $viewParams = [
<? if ($useJs || $useAngularJsComponents || $useAngularJsController) { ?>
            'jsParams' => [
                'param' => $jsParamValue,
            ],
<? } ?>
            'param' => $paramValue,
        ];

        return $this->render('news', $viewParams);
    }
}