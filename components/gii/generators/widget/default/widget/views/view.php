<?= '<?php' ?>

/** @var \yii\web\View $this Представление */
<? if ($useJs || $useAngularJsComponents || $useAngularJsController) { ?>
/** @var array $jsParams Параметры для JS */
/** @var string $jsParamsJson Параметры для JS в JSON формате */
<? } ?>
<?= '?>' ?>


<div<?= ($useJs ? ' id="<?= $widgetId ?>"' : null)?>>

</div>

<? if ($useJs) { ?>
<?= '<?' ?>

$this->registerJs(<<< JS
    var widget = jQuery('#$widgetId');
    var widgetParams = $jsParamsJson;
JS
);
<? } ?>