<?php


namespace app\components\authClients;

use yii\authclient\OAuth2;

class Odnoklassniki extends OAuth2
{
    // :TODO Работа обновления токена не проверена, возможно нужна отправка подписи так же, как и при обращении к API
    public $authUrl = 'https://www.odnoklassniki.ru/oauth/authorize';
    public $tokenUrl = 'http://api.odnoklassniki.ru/oauth/token.do';
    public $apiBaseUrl = 'http://api.odnoklassniki.ru/api';
    /** @var string Публичный ключ приложения */
    public $clientPublic;
    /** @var string Название вызываемого API-метода */
    protected $apiMethodName = null;

    protected function initUserAttributes()
    {
        return $this->api('users/getCurrentUser', 'GET');
    }

    protected function defaultName()
    {
        return 'odnoklassniki';
    }

    protected function defaultTitle()
    {
        return 'Одноклассники';
    }

    /**
     * @inheritdoc
     */
    protected function defaultNormalizeUserAttributeMap()
    {
        return [
            'id' => 'uid'
        ];
    }

    /**
     * @inheritdoc
     */
    public function api($apiSubUrl, $method = 'GET', array $params = [], array $headers = [])
    {
        $this->apiMethodName = $apiSubUrl;

        return parent::api($apiSubUrl, $method, $params, $headers);
    }

    /**
     * @inheritdoc
     */
    protected function apiInternal($accessToken, $url, $method, array $params, array $headers)
    {
        $params['application_key'] = $this->clientPublic;
        $params['method'] = $this->apiMethodName;
        /** Подпись запроса, генерация должна содержать ВСЕ параметры, кроме access token */
        $params['sig'] = $this->generateSign($params);
        $params['access_token'] = $accessToken->getToken();

        return $this->sendRequest($method, $url, $params, $headers);
    }

    /**
     * Генерация подписи, частично взято из официальной библиотеки
     * @param array $parameters
     * @return null|string
     */
    protected function generateSign($parameters = [])
    {
        if (!ksort($parameters)) {
            return null;
        } else {
            $requestStr = "";
            foreach ($parameters as $key => $value) {
                $requestStr .= $key . "=" . $value;
            }
            $requestStr .= md5($this->accessToken->getToken() . $this->clientSecret);

            return md5($requestStr);
        }
    }
}