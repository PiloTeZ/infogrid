<?php


namespace app\components;

use Yii;
use yii\base\Object;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;

/**
 * Переадресует пользователя по определённому маршруту
 */
// TODO Проверить
// Получение из GET параметров проверено
class AutoRedirect extends Object
{
    const ROUTE_SOURCE_GET = 'get';
    const ROUTE_SOURCE_POST = 'post';
    // GET & POST
    const ROUTE_SOURCE_REQUEST = 'request';
    // Брать маргрут для переадресации из переданного машрута (из свойств этого объекта)
    const ROUTE_SOURCE_PROPERTIES = 'properties';

    public $routeSource;
    // Если $routeSource == ROUTE_SOURCE_PROPERTIES
    public $route;
    // Если $routeSource == ROUTE_SOURCE_REQUEST, ROUTE_SOURCE_POST, ROUTE_SOURCE_GET
    /** @var string Имя свойства в $_GET, $_POST, $_RESPONSE, которое содержит маршрут для переадресации */
    public $routeRequestKey = 'redirect';
    /** @var string Имя свойства в $_GET, $_POST, $_RESPONSE, которое содержит параметры маршрута для переадресации */
    public $routeParamsRequestKey = 'redirect_params';

    /** @var array Разрешенные маршруты */
    public $allowedRoutes = [];
    /** @var array Запрещённые маршруты */
    public $permittedRoutes = [];

    /**
     * @param array $routeSource
     * @param null $route Используется при $routeSource ROUTE_SOURCE_PROPERTIES
     * @param array $config
     */
    public function __construct($routeSource = null, $route = null, $config = [])
    {
        $this->routeSource = (empty($routeSource) ? $this::ROUTE_SOURCE_REQUEST : $routeSource);
        if (!empty($route)) {
        }
        $this->route = $route;

        parent::__construct($config);
    }

    public function init()
    {
        $this->redirect();
    }

    protected function redirect()
    {
        $route = $this->getRoute();

        if (!empty($route)) {
            Yii::$app->getResponse()->redirect($route);
        }
    }

    protected function getRoute()
    {
        if ($this->routeSource == $this::ROUTE_SOURCE_REQUEST) {
            // CHECK
            $route = $this->getRouteFromRequest(Yii::$app->request->get() + Yii::$app->request->post());
        } else if ($this->routeSource == $this::ROUTE_SOURCE_GET) {
            $route = $this->getRouteFromRequest(Yii::$app->request->get());
        } else if ($this->routeSource == $this::ROUTE_SOURCE_POST) {
            $route = $this->getRouteFromRequest(Yii::$app->request->post());
        } else if ($this->routeSource == $this::ROUTE_SOURCE_PROPERTIES) {
            $route = $this->getRouteFromProperties();
        } else {
            throw new ServerErrorHttpException('Неверный источник свойств передаресации');
        }

        return $route;
    }

    protected function getRouteFromRequest($request)
    {
        if (empty($this->routeRequestKey) || empty($this->routeParamsRequestKey)) {
            throw new ServerErrorHttpException('Неверные параметры переадресации');
        }
        $routePath = $request[$this->routeRequestKey];
        $routeParams = $request[$this->routeParamsRequestKey];

        if (!is_string($routePath) || (!empty($routeParams) && !is_array($routeParams))) {
            return false;
        }

        $route = [$routePath];
        if (is_array($routeParams)) {
            $route = ArrayHelper::merge($route, $routeParams);
        }

        return $route;
    }

    protected function getRouteFromProperties()
    {
        if (!empty($this->route) && !is_array($this->route)) {
            throw new ServerErrorHttpException('Неверные параметры переадресации');
        }

        return $this->route;
    }
}