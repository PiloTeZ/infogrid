<?php
namespace app\components;

use yii\base\Object;

/**
 * Класс для управления сущностями исключительно текущего пользователя
 */
class BaseUserEntitiesManager extends Object
{
    /** @var \yii\web\User Владелец Сущностей */
    protected $user;

    public function init()
    {
        $this->user = \Yii::$app->user;
        if ($this->user->getIsGuest()) {
            throw new \Exception('Требуется авторизация');
        }
    }
}