<?php
namespace app\components;

class ActiveQuery extends \yii\db\ActiveQuery
{
    public function id($id)
    {
        $this->andWhere(['id' => $id]);

        return $this;
    }
}