<?php


namespace app\components;

use Yii;
use yii\base\Component;

class FlashMessages extends Component
{
    public function addSuccess($text)
    {
        Yii::$app->session->setFlash('success', $text);
    }

    public function addError($text)
    {
        Yii::$app->session->setFlash('error', $text);
    }

    public function addWarning($text)
    {
        Yii::$app->session->setFlash('warning', $text);
    }

    public function addInfo($text)
    {
        Yii::$app->session->setFlash('info', $text);
    }

    public function addDanger($text)
    {
        Yii::$app->session->setFlash('danger', $text);
    }
}