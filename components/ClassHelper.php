<?php


namespace app\components;


use yii\base\Object;

/**
 * Хелпер для работы с классами
 * @package app\components
 */
class ClassHelper extends Object
{
    protected $class;
    protected $className;
    protected $classNamespace;
    protected $classPath;

    /**
     * @param Object|string $class Полное название класса (с неймспейсом)
     * @param array $params
     */
    public function __construct($class, $params = [])
    {
        parent::__construct($params);

        $this->class = ($class instanceof Object ? $class::className() : $class);
        $classInfo = $this->parseClassName();
        $this->className = $classInfo['name'];
        $this->classNamespace = $classInfo['namespace'];
    }

    /**
     * Имя класса
     * @return string
     */
    public function getName()
    {
        return $this->className;
    }

    /**
     * Неймспейс
     * @return string
     */
    public function getNamespace()
    {
        return $this->classNamespace;
    }

    /**
     * Путь к папке, в которой находится. Используется формат пути Yii
     * @return string
     */
    public function getPath()
    {
        $classPath = $this->classPath;
        if (empty($classPath)) {
            $classPath = str_replace('\\', '/', $this->classNamespace) . '/';
            if ($classPath[0] == '/') {
                $classPath[0] = '@';
            } else {
                $classPath = '@' . $classPath;
            }
        }

        return $classPath;
    }

    /**
     * Разбиение полного названия класса на имя и неймспейс
     * @return array
     */
    protected function parseClassName()
    {
        $matches = [];
        preg_match('/([\A-z]+)\\\([A-z]+)$/i', $this->class, $matches);
        return ['name' => $matches[2], 'namespace' => $matches[1]];
    }
}