<?php


namespace app\components;


use app\widgets\base\BaseWidget;
use yii\base\Object;

class WidgetHelper extends Object
{
    protected $widgetClass;
    protected $classHelper;
    protected $widgetName;
    protected $widgetFullName;

    /**
     * @param BaseWidget|string $class
     * @param array $params
     */
    public function __construct(BaseWidget $class, $params = [])
    {
        parent::__construct($params);
        $this->widgetClass = $class;
        $this->classHelper = new ClassHelper($class);
    }

    /**
     * Имя виджета (~ News)
     * @return string
     */
    public function getName()
    {
        if (empty($this->widgetName)) {
            $className = $this->classHelper->getName();
            $this->widgetName = str_replace('Widget', '', $className);
        }
        return $this->widgetName;
    }

    /**
     * Полное имя класса (~ NewsWidget)
     * @return string
     */
    public function getFullName()
    {
        return $this->classHelper->getName();
    }

    /**
     * Неймспейс
     * @return string
     */
    public function getNamespace()
    {
        return $this->classHelper->getNamespace();
    }

    /**
     * Путь к папке, в которой находится виджет. Используется формат пути Yii
     * @return string
     */
    public function getPath()
    {
        return $this->classHelper->getPath();
    }

    /**
     * Путь к папке, в которой находятся ресурсы виджета. Используется формат пути Yii
     * @return string
     */
    public function getAssetsPath()
    {
        return $this->getPath() . 'assets/';
    }

    function getAssetClass()
    {
        return $this->getNamespace() . '\\assets\\' . $this->getFullName() . 'Asset';
    }
}