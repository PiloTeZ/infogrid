<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\CommonAsset;
use \app\widgets\AlertWidget;
use \app\assets\AngularJsModuleAsset;

/* @var $this \yii\web\View */
/* @var $content string */

CommonAsset::register($this);
AngularJsModuleAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" ng-app="InfoGrid">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'InfoGrid.ru',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $items = [];

    if (Yii::$app->user->isGuest) {
        $items[] = ['label' => 'Войти', 'url' => ['/users/email-account/authorize']];
        $items[] = ['label' => 'Зарегистрироваться', 'url' => ['/users/email-account/register']];
    } else {
        $items = [
            [
                'label' => '<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> Добавить виджет',
                'encode' => false,
                'url' => ['/widgets/catalog/list']
            ],
            ['label' => 'Аккаунт', 'items' => [
                ['label' => 'Настройки', 'url' => ['']],
                ['label' => 'Выйти', 'url' => ['/users/authorizations/sign-out'], 'linkOptions' => ['data-method' => 'post']],
            ],
            ],
        ];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= AlertWidget::widget() ?>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
