<?
use yii\helpers\Html;
use app\assets\CommonAsset;
use app\assets\AngularJsModuleAsset;
use \app\assets\InnerLayoutAsset;

// TRICKY Модальные окна для кнопок шаблона должны распологаться прямо в шаблоне в виде виджетов
// обернутых в виджет модального окна!
CommonAsset::register($this);
AngularJsModuleAsset::register($this);
InnerLayoutAsset::register($this);
if (empty($this->title)) {
	$this->title = 'InfoGrid.dev';
}
?>
<? $this->beginPage() ?>
	<!DOCTYPE html>
	<html ng-app="InfoGrid">
	<head lang="<?= Yii::$app->language ?>">
		<meta charset="<?= Yii::$app->charset ?>"/>
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<? $this->head() ?>
	</head>
	<body>
	<? $this->beginBody() ?>
	<div class="page">
		<div class="page__header page-header">
			<div class="page-header__logo page-header-logo">
				<a href="/" class="page-header-logo__link">
					<img src="/images/logo.png" alt="InfoGrid.ru" class="page-header-logo__image">
				</a>
			</div>
			<div class="page-header__menu page-header-menu">
				<div class="page-header-menu__item">Добавить гаджет</div>
				<div class="page-header-menu__item">Настройки</div>
			</div>
		</div>
		<div class="page__content page-content">
			<?= $content ?>
		</div>
	</div>
	<? $this->endBody() ?>
	</body>
	</html>
<? $this->endPage() ?>