<?php


namespace app\modules\users\controllers;

use Yii;
use yii\web\Controller;
use app\modules\users\components\AccountsManager;
use app\components\AutoRedirect;

class AccountsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // Доступно только авторизованным пользователям
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $accountsManager = new AccountsManager;
        $accounts = $accountsManager->getAccounts(Yii::$app->user->id);

        return $this->render('accounts', ['accounts' => $accounts]);
    }

    public function actionDelete($account_type, $account_id)
    {
        $accountsManager = new AccountsManager;
        // TODO
        if ($accountsManager->delete(Yii::$app->user->identity, $account_type, $account_id)) {
            Yii::$app->flashMessages->addSuccess('Аккаунт удалён');
        } else {
            Yii::$app->flashMessages->addError('Аккаунт не удалён');
        }

        new AutoRedirect();
    }
}