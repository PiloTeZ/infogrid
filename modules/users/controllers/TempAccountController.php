<?php

namespace app\modules\users\controllers;

use Yii;
use yii\web\Controller;
use app\modules\users\models\entities\TempAccount;
use app\modules\users\components\TempAccountsManager;
use app\components\AutoRedirect;

class TempAccountController extends Controller
{
    // CHECK
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['register', 'authorize'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
        ];
    }

    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/widgets/widgets/list']);
        }

        $accountManager = new TempAccountsManager();
        $regInfo = $accountManager->register();
        if ($regInfo && $accountManager->authorize($regInfo['account'])) {
            Yii::$app->flashMessages->addSuccess('Регистрация прошла успешно! Аккаунт временный и будет удалён через 7 дней');
            // FUTURE Выводить данное предупреждение при каждом начале новой сессии
            // FUTURE Заставлять пользователя зарегистрироваться на 3-й день

            return $this->redirect(['/widgets/widgets/list']);
        } else {
            Yii::$app->flashMessages->addError('Ошибка при регистрации временного аккаунта. Пожалуйста зарегистрируйтесь вручную');
            Yii::error('Ошибка при регистрации временного аккаунта');

            return $this->goHome();
        }
    }

    public function actionAuthorize($auth_key)
    {
        // CHECK Проверить все возомжные исходы (неверный ключ, пустой ключ...)
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/widgets/widgets/list']);
        }

        $accountsManager = new TempAccountsManager();
        $account = new TempAccount();
        $account->authKey = $auth_key;
        if ($accountsManager->authorize($account)) {
            Yii::$app->flashMessages->addSuccess('Авторизация прошла успешно!');

            return $this->redirect(['/widgets/widgets/list']);
        } else {
            Yii::$app->flashMessages->addError('Авторизаця не удалась');
        }

        new AutoRedirect();
    }
}