<?php


namespace app\modules\users\controllers;

use app\modules\users\components\AuthorizationsManager;
use Yii;
use yii\web\Controller;
use yii\web\ServerErrorHttpException;
use yii\filters\VerbFilter;

class AuthorizationsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'sign-out' => ['post'],
                ],
            ],
        ];
    }

    public function actionList()
    {
        throw new ServerErrorHttpException('Метод не реализован');
    }

    /**
     * Сбросить текущую авторизацию
     * @return \yii\web\Response
     */
    public function actionSignOut()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $authManager = new AuthorizationsManager();
        if (!$authManager->logout()) {
            Yii::$app->flashMessages->addError('Выход из аккаунта не удался');
        }

        return $this->goHome();
    }

    /**
     * Выход со всех устройств
     */
    public function actionFullSignOut()
    {
        // То же самое что и обычный выход, только обновляется код авторизации
        throw new ServerErrorHttpException('Метод не реализован');
    }
}