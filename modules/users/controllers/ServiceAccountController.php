<?php

namespace app\modules\users\controllers;

use Yii;
use yii\web\Controller;
use app\modules\users\components\ServiceAccountsManager;

class ServiceAccountController extends Controller
{
    // CHECK
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['authorize', 'authorizeService'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['assignService'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'authorizeService' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'authorize'],
            ],
            'assignService' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'assign'],
            ],
        ];
    }

    public function authorize($serviceData)
    {
        $accountsManager = new ServiceAccountsManager;
        if ($accountsManager->authorize($serviceData)) {
            Yii::$app->flashMessages->addSuccess('Авторизация прошла успешно!');
        } else if ($accountsManager->register($serviceData) && $accountsManager->authorize($serviceData)) {
            Yii::$app->flashMessages->addSuccess('Регистрация прошла успешно!');
        } else {
            Yii::$app->flashMessages->addError('Авторизация не удалась');

            return $this->goHome();
        }

        // Переадресация срабатывает в окошке
        //return $this->redirect(['/widgets/widgets/list']);
    }

    public function assign($serviceData)
    {
        $accountsManager = new ServiceAccountsManager;
        if ($accountsManager->register($serviceData) && $accountsManager->authorize($serviceData)) {
            Yii::$app->flashMessages->addSuccess('Аккаунт ' . $serviceData->title . ' успешно привязан!');
        } else {
            Yii::$app->flashMessages->addError('Не удалось привязать аккаунт');
        }

        //return $this->redirect(['/users/accounts/list']);
    }

    public function actionAuthorize()
    {
        return $this->render('services');
    }
}