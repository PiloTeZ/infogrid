<?php


namespace app\modules\users\controllers;

use app\components\AutoRedirect;
use yii\web\Controller;

class TestController extends Controller
{
    public function actionTest()
    {
        new AutoRedirect();
    }
}