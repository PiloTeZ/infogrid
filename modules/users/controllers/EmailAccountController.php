<?php

namespace app\modules\users\controllers;

use Yii;
use yii\web\Controller;
use app\modules\users\models\entities\EmailAccount;
use app\modules\users\components\EmailAccountsManager;

class EmailAccountController extends Controller
{
    // FUTURE Убрать проверки авторизации из методов
    // CHECK
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['register', 'authorize'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['assign'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['password-recovery'],
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
        ];
    }

    public function actionRegister()
    {
        $account = new EmailAccount(['scenario' => EmailAccount::SCENARIO_REGISTRATION]);
        if ($account->load(Yii::$app->request->post())) {
            $accountManager = new EmailAccountsManager();
            if ($accountManager->register($account)) {
                Yii::$app->flashMessages->addSuccess('Регистрация прошла успешно!');

                if (!$accountManager->authorize($account)) {
                    Yii::$app->flashMessages->addError('Авторизация не удалась');

                    return $this->redirect(['/users/email-account/authorize']);
                }

                return $this->redirect(['/widgets/widgets/list']);
            } else {
                Yii::$app->flashMessages->addError('При регистрации возникла ошибка');
            }
        }

        return $this->render('registration', ['account' => $account]);
    }

    public function actionAuthorize()
    {
        $account = new EmailAccount(['scenario' => EmailAccount::SCENARIO_AUTHORIZATION]);
        if ($account->load(Yii::$app->request->post())) {
            $accountManager = new EmailAccountsManager();
            if ($accountManager->authorize($account)) {
                Yii::$app->flashMessages->addSuccess('Авторизация прошла успешно!');

                return $this->redirect(['/widgets/widgets/list']);
            } else {
                Yii::$app->flashMessages->addError('Авторизаця не удалась. Проверьте правильность введённых данных');
            }
        }

        return $this->render('authorization', ['account' => $account]);
    }

    public function actionAssign()
    {
        $account = new EmailAccount(['scenario' => EmailAccount::SCENARIO_REGISTRATION]);
        if ($account->load(Yii::$app->request->post())) {
            $accountManager = new EmailAccountsManager();
            if ($accountManager->assign($account, Yii::$app->user->identity)) {
                Yii::$app->flashMessages->addSuccess('Аккаунт успешно привязан!');

                return $this->redirect(['/users/accounts/list']);
            } else {
                Yii::$app->flashMessages->addError('Не удалось привязать аккаунт');
            }
        }

        return $this->render('assign', ['account' => $account]);
    }

    public function actionDelete($accountId)
    {
        $accountsManager = new EmailAccountsManager();
        $accountsManager->delete(Yii::$app->user->identity, $accountId);
    }

    public function actionPasswordRecovery()
    {
        $accountsManager = new EmailAccountsManager;
        $passwordRecoveryData = new EmailAccount(['scenario' => EmailAccount::SCENARIO_RECOVERY]);
        if ($passwordRecoveryData->load(Yii::$app->request->post())) {
            if ($accountsManager->recoverPassword($passwordRecoveryData)) {
                Yii::$app->flashMessages->addSuccess('На Ваш email отправлена ссылка для восстановления пароля');

                return $this->goHome();
            } else {
                Yii::$app->flashMessages->addError('При отправке сообщения на этот email возникла ошибка');
            }
        }

        return $this->render('password-recovery', ['passwordRecoveryData' => $passwordRecoveryData]);
    }
}