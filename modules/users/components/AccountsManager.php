<?php


namespace app\modules\users\components;

use Yii;
use yii\base\Object;
use app\modules\users\models\entities\User;
use yii\helpers\Inflector;
use yii\web\ForbiddenHttpException;

class AccountsManager extends Object
{
    // FUTURE Сделать так, что бы аккаунты не из конфигурации были запрещены к использованию
    // :TODO Вынести в конфигурацию
    /** @var array Список типов аккаунтов [key => namespace] */
    public $accounts = [
        'email' => 'app\modules\users\models\entities\EmailAccount',
        'service' => 'app\modules\users\models\entities\ServiceAccount',
        'temp' => 'app\modules\users\models\entities\TempAccount',
    ];
    public $accountManagersNamespace = 'app\modules\users\components';

    /**
     * Получение всех аккаунтов. Возможна фильтрация по пользователю
     * :WARNING Значение по-умолчанию для параметра $userId не установлено нарошно
     * Что бы случайно не получить все аккаунты, когда это не нужно
     * @param $userId integer|null
     * @return array
     */
    public function getAccounts($userId)
    {
        $accounts = [];

        foreach ($this->accounts as $name => $namespace) {
            $query = $namespace::find();
            if (!empty($userId)) {
                $query->userId($userId);
            }

            $accounts += $query->all();
        }

        // CHECK
        Yii::trace('Получение всех аккаунтов пользователя ' . $userId);
        Yii::info('Получено ' . count($accounts) . ' аккаунтов');

        return $accounts;
    }

    /**
     * Получение менеджера аккаунтов определённого типа
     * @param $accountType
     * @return string
     */
    public function getAccountsManagerClass($accountType)
    {
        $accountsManagerClass = $this->accountManagersNamespace . '\\' . Inflector::id2camel($accountType) . 'AccountsManager';

        return (class_exists($accountsManagerClass) ? $accountsManagerClass : false);
    }

    public function delete(User $user, $accountType, $accountId)
    {
        $account = $this->getAccount($accountType, $accountId);

        if (!$user->validate() || empty($account) || $account->user_id != $user->id) {
            return false;
        }

        if ($this->getAccountsNumber($user->id) <= 1) {
            throw new ForbiddenHttpException('Нельзя удалить последний аккаунт');
        }

        return $account->delete();
    }

    /**
     * Получение аккаунта
     * @param $accountType
     * @param $accountId
     * @return mixed
     */
    public function getAccount($accountType, $accountId)
    {
        $entity = $this->accounts[$accountType];

        return $entity::findOne((int) $accountId);
    }

    /**
     * Получение количества всех аккаунтов. Возможна фильтрация по пользователю
     * :WARNING Значение по-умолчанию для параметра $userId не установлено нарошно
     * Что бы случайно не получить все аккаунты, когда это не нужно
     * @param $userId integer|null
     * @return integer
     */
    public function getAccountsNumber($userId)
    {
        $accountsNumber = [];

        foreach ($this->accounts as $name => $namespace) {
            $query = $namespace::find();
            if (!empty($userId)) {
                $query->userId($userId);
            }

            $accountsNumber += $query->count();
        }
        // CHECK
        Yii::trace('Подсчёт всех аккаунтов пользователя ' . $userId);
        Yii::info('Найдено ' . $accountsNumber . ' аккаунтов');

        return $accountsNumber;
    }
}