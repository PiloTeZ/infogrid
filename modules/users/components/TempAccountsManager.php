<?php


namespace app\modules\users\components;
// TODO Доделать привязку аккаунтов
use Yii;
use app\modules\users\models\entities\TempAccount;
use app\modules\users\models\entities\User;
use yii\base\Security;
// FUTURE Помечать дату авторизации
class TempAccountsManager extends AbstractAccountsManager
{
    // FUTURE Вынести в конфигурацию
    /** @var int Время жизни акканта при пробной регистрации и по умолчанию */
    public $defaultExpireDuration = 2592000;

    /**
     * @param TempAccount $account
     * @param null $expireDuration
     * @return array|bool
     */
    public function register(TempAccount $account = null, $expireDuration = null)
    {
        return $this->_register(null, $account, $expireDuration);
    }

    /**
     * @param $userData
     * @param TempAccount $account
     * @param $expiresDuration
     * @return mixed
     */
    public function assign($userData, TempAccount $account = null, $expiresDuration = null)
    {
        return $this->_register($userData, $account, $expiresDuration);
    }

    /**
     * @param null $userData
     * @param TempAccount $account
     * @param $expireDuration
     * @return array|bool
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\ServerErrorHttpException
     */
    protected function _register($userData = null, TempAccount $account = null, $expireDuration = null)
    {
        Yii::trace('Регистрация временного аккаунта');
        if (empty($expireDuration)) {
            $expireDuration = $this->defaultExpireDuration;
        }
        if (empty($account)) {
            $account = new TempAccount();
        }
        $account->setScenario(TempAccount::SCENARIO_REGISTRATION);

        $usersManager = new UsersManager();
        $security = new Security;
        /* Для удобства */
        $user = $this->getUserByUserData($userData);
        if (empty($user)) {
            return false;
        }

        $secretAuthKey = $security->generateRandomString();
        $account->user_id = $user->id;
        // Не сохраняется. Сделано для использования после регистрации вне метода
        $account->authKey = $secretAuthKey;
        $account->auth_key_hash = $security->hashData($secretAuthKey, TempAccount::AUTH_KEY_HASH_KEY);
        // TODO Указание времени жизни аккаунта (смена пароля - 1 день, временная регистрация - неделя)
        $account->expires_time = time() + $expireDuration;
        $account->registration_time = time();
        $accountCreated = $account->insert();

        /* Если пользователь создан в этом методе и не удалось зарегистрировать аккаунт,
         то удаляем пользователя */
        if (!$accountCreated) {
            if (empty($user->oldAttributes)) {
                $usersManager->delete($user);
            }
            Yii::error('Регистрация не удалась. Ошибка при создании временного аккаунта');

            return false;
        }

        return ['user' => $user, 'account' => $account];
    }

    /**
     * @param TempAccount $accountData
     * @return bool
     */
    public function authorize(TempAccount $accountData)
    {
        $accountData->setScenario(TempAccount::SCENARIO_AUTHORIZATION);
        if (!$accountData->validate()) {
            return false;
        }

        Yii::trace('Авторизация временного аккаунта');

        $authManager = new AuthorizationsManager;
        $account = $this->authenticate($accountData);
        if (!empty($account) && !empty($account->user)) {
            // Удаление ключа авторизации после первой авторизации для безопасности
            $this->deleteAuthKey($account, $account->user);

            return $authManager->authorize($account->user);
        } else {
            return false;
        }
    }

    /**
     * @param TempAccount $accountData
     * @return array|bool|null|\app\models\Entity
     * @throws \yii\base\InvalidConfigException
     */
    public function authenticate(TempAccount $accountData)
    {
        $authenticateData = clone $accountData;
        $authenticateData->setScenario(TempAccount::SCENARIO_AUTHENTICATION);
        if (!$accountData->validate()) {
            Yii::warning('Аутентификация не удалась. Переданы неверные данные');

            return false;
        }

        Yii::trace('Аутентификация временного аккаунта');
        $account = TempAccount::find()->authKey($accountData->authKey)->one();

        return (!empty($account) ? $account : false);
    }

    public function deleteAuthKey(TempAccount $account, User $user)
    {
        // CHECK
        if ($user->id != $account->user_id) {
            return false;
        }

        $account->auth_key_hash = null;

        return $account->update();
    }
}