<?php


namespace app\modules\users\components;

use Yii;
use app\modules\users\models\entities\User;
use yii\base\Model;
use app\modules\users\models\data\UserData;

class UsersManager extends Model
{
    /**
     * @param User $user
     * @return User|bool
     * @throws \Exception
     */
    public function create(User $user = null)
    {
        Yii::trace('Создание пользователя');
        if (empty($user)) {
            $user = new User;
        }

        $user->setScenario(User::SCENARIO_CREATION);

        $user->registration_time = time();
        if (!$user->validate()) {
            return false;
        }

        return ($user->insert() ? $user : false);
    }

    // FUTURE
    /*
    public function delete(User $user)
    {
        Yii::trace('Удаление пользователя');
        Удаление связанных данных
        //return $user->delete();
        return false;
    }
    */
}