<?php


namespace app\modules\users\components;

use yii\authclient\BaseClient;
use yii\helpers\Inflector;
use yii\web\ServerErrorHttpException;
use Yii;

class ServicesManager
{
    public $normalizersNamespace = 'app\modules\users\models\helpers\userDataNormalizers';
    public $providersCollection = 'authClientCollection';

    /**
     * Нормализация данных о пользователе от сервиса
     * @param BaseClient $service
     * @return \app\modules\users\models\entities\UserData
     * @throws ServerErrorHttpException
     */
    public function getNormalizedUserData(BaseClient $service)
    {
        $userData = $service->userAttributes;
        $normalizeClass = $this->getUserDataNormalizerClass($service->id);
        if (!class_exists($normalizeClass)) {
            throw new ServerErrorHttpException('Нормализатор данных аккаунта не найден');
        }
        $normalizer = new $normalizeClass($userData);
        $normalizedData = $normalizer->normalize();

        return $normalizedData;
    }

    /**
     * Класс нормализатор
     * @param $serviceId string
     * @return string
     */
    public function getUserDataNormalizerClass($serviceId)
    {
        return $this->normalizersNamespace . '\\' . $this->getServiceCamelId($serviceId) . 'UserDataNormalizer';
    }

    /**
     * Получение сервиса по идентификатору
     * @param $serviceId
     * @return mixed
     */
    public function getService($serviceId)
    {
        $collection = $this->getProvidersCollection();
        $client = $collection->getClient($serviceId);

        return $client;
    }

    /**
     * @param $serviceId string
     * @return string
     */
    public function getServiceCamelId($serviceId)
    {
        return Inflector::id2camel($serviceId);
    }

    /**
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    public function getProvidersCollection()
    {
        return Yii::$app->get($this->providersCollection);

    }
}