<?php


namespace app\modules\users\components;

use app\modules\users\models\entities\AccountInterface;
use Yii;
use app\modules\users\models\entities\ServiceAccount;
use yii\authclient\BaseClient;
use yii\authclient\BaseOAuth;
use yii\base\Object;
use app\modules\users\models\entities\User;
use app\modules\users\models\entities\UserData;
use yii\web\ServerErrorHttpException;

class ServiceAccountsManager extends AbstractAccountsManager
{
    public function register(BaseOAuth $serviceData)
    {
        return $this->_register($serviceData);
    }

    public function assign(BaseOAuth $serviceData, $userData)
    {
        return $this->_register($serviceData, $userData);
    }

    /**
     * @param BaseOAuth $serviceData
     * @param null $userData
     * @return array|bool
     * @throws \Exception
     * @throws \yii\web\ServerErrorHttpException
     */
    protected function _register(BaseOAuth $serviceData, $userData = null)
    {
        $account = new ServiceAccount;
        $account->setScenario(ServiceAccount::SCENARIO_REGISTRATION);

        $usersManager = new UsersManager;

        if ($this->authenticate($serviceData)) {
            return false;
        }

        $user = $this->getUserByUserData($userData);
        if (empty($user)) {
            return false;
        }

        $account->user_id = $user->id;
        $account->service_text_id = $serviceData->id;
        $account->service_user_id = $serviceData->userAttributes['id'];
        $this->populateAccountToken($account, $serviceData);
        $accountCreated = $account->insert();

        /* Если пользователь создан в этом методе и не удалось зарегистрировать аккаунт,
         то удаляем пользователя */
        if (!$accountCreated) {
            if (empty($user->oldAttributes)) {
                $usersManager->delete($user);
            }

            return false;
        }

        $this->saveUserData($account, $user, $serviceData);

        return ['user' => $user, 'account' => $account];
    }

    /**
     * @param BaseOAuth $serviceData
     * @return ServiceAccount|bool
     */
    public function authenticate(BaseOAuth $serviceData)
    {
        Yii::trace('Аутентификация ' . $serviceData->title);
        $account = ServiceAccount::find()->serviceTextId($serviceData->id)->serviceUserId($serviceData->userAttributes['id'])->one();

        if (!empty($account)) {
            return $account;
        } else {
            Yii::warning('Аутентификация не удалась');

            return false;
        }
    }

    /**
     * @param BaseOAuth $serviceData
     * @param bool $anotherDevice
     * @return bool
     */
    public function authorize(BaseOAuth $serviceData, $anotherDevice = false)
    {
        Yii::trace('Авторизация');
        $authManager = new AuthorizationsManager;
        $account = $this->authenticate($serviceData);
        $account->setScenario(ServiceAccount::SCENARIO_AUTHORIZATION);

        if (!empty($account) || !$account->validate()) {
            if (empty($account->user)) {
                throw new ServerErrorHttpException('Пользователь не найден');
            }
            $this->updateToken($serviceData, $account);
            $this->saveUserData($account, $account->user, $serviceData);

            return $authManager->authorize($account->user, !$anotherDevice);
        } else {
            return false;
        }
    }

    /**
     * @param BaseOAuth $serviceData
     * @param ServiceAccount $account
     * @return bool|int
     * @throws \Exception
     */
    public function updateToken(BaseOAuth $serviceData, ServiceAccount $account)
    {
        Yii::trace('Обновление токена');
        $account->setScenario(ServiceAccount::SCENARIO_TOKEN_UPDATING);

        return ($this->populateAccountToken($account, $serviceData) ? $account->update() : false);
    }

    /**
     * Соранение данных пользователя
     * @param AccountInterface $account
     * @param User $user
     * @param BaseOAuth $serviceData
     * @throws \yii\web\ServerErrorHttpException
     */
    public function saveUserData(AccountInterface $account, User $user, BaseOAuth $serviceData)
    {
        $servicesManager = new ServicesManager;
        $usersDataManager = new UsersDataManager;
        $accountData = $servicesManager->getNormalizedUserData($serviceData);
        $accountData->user_id = $user->id;
        $accountData->source = UserData::SOURCE_ACCOUNT_SERVICE;
        $accountData->account_id = $account->id;
        if (!$usersDataManager->save($accountData)) {
            Yii::error('Данные о пользователе от сервиса не сохранены');
        }
    }

    public function populateAccountToken(ServiceAccount $account, BaseOAuth $serviceData)
    {
        if ($serviceData->accessToken->getIsValid()) {
            $tokenSecret = (isset($serviceData->accessToken->tokenSecret) ? $serviceData->accessToken->tokenSecret : null);
            return $account->populateToken(
                $serviceData->accessToken->token,
                $tokenSecret,
                $serviceData->accessToken->createTimestamp,
                $serviceData->accessToken->expireDuration
            );
        } else {
            return false;
        }
    }
}