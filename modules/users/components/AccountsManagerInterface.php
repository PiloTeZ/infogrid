<?php


namespace app\modules\users\components;

use app\modules\users\models\entities\User;

interface AccountsManagerInterface
{
    function delete(User $user, $accountId);
}