<?php

namespace app\modules\users\components;

use Yii;
use yii\base\Object;
use app\modules\users\models\entities\User;

class AuthorizationsManager extends Object
{
    public $rememberDuration = 31000000;

    /**
     * Авторизация указанного пользователя
     * @param User $user
     * @param bool $remember
     * @return bool
     */
    public function authorize(User $user, $remember = true)
    {
        $user->setScenario(User::SCENARIO_AUTHORIZATION);
        $rememberDuration = ($remember ? $this->rememberDuration : 0);

        Yii::trace('Авторизация пользователя ' . $user->id);
        Yii::info('Длительность авторизации ' . $rememberDuration);

        $user->authorization_time = time();
        $user->update();

        return Yii::$app->user->login($user, $rememberDuration);
    }

    /**
     * Разавторизация текущего пользователя
     * @return bool
     */
    public function logout()
    {
        Yii::trace('Развторизация текущего пользователя');

        return Yii::$app->user->logout();
    }
}