<?php


namespace app\modules\users\components;

use Yii;
use app\modules\users\models\entities\User;
use yii\base\Object;
use yii\web\ServerErrorHttpException;
use yii\helpers\Inflector;

// FUTURE Удаление временных аккаунтах при авторизации любым другим методом
// FUTURE Прописать удаление временных аккаунтов в кроне
abstract class AbstractAccountsManager extends Object
{
    /**
     * @param User $user
     * @param $accountId
     * @return bool
     * @throws \yii\web\ForbiddenHttpException
     */
    function delete(User $user, $accountId)
    {
        $accountsManager = new AccountsManager;

        return $accountsManager->delete($user, $this->getAccountManagerType(), $accountId);
    }

    public function getAccountManagerType()
    {
        $managerClassName = $this->className();
        $entityNameStart = strrpos('\\', $managerClassName);
        $entityNameEnd = stripos('AccountsManager', $managerClassName);

        return Inflector::camel2id(substr($managerClassName, $entityNameStart, $entityNameEnd));
    }

    /**
     * @param User|integer $userData
     * $userData integer Вернёт пользователя по идентификатору
     * $userData User if (User::isNewRecord) Создаст пользователя на основе полученной модели
     * $userData User if (!User::isNewRecord) Валидирует и вернёт данные пользователя
     * @param bool $required
     * @return User|bool
     */
    protected function getUserByUserData($userData, $required = true)
    {
        $user = false;
        $usersManager = new UsersManager;

        if (empty($userData)) {
            $userData = new User;
        }

        if ($userData instanceof User) {
            // Создание пользователя на основе $userData
            if ($userData->isNewRecord) {
                $user = $usersManager->create($userData);
                // Проверка переданных данных о существующем пользователе
            } else if (!$userData->validate()) {
                $user = false;
            }
            // Поиск по идентификатору
        } else if (is_integer($userData)) {
            $user = User::findOne($userData);
        }

        if ($required == true && empty($user)) {
            throw new ServerErrorHttpException('Пользователь для создания аккаунта не найден');
        }

        return $user;
    }
}