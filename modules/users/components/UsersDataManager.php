<?php


namespace app\modules\users\components;

use app\modules\users\models\entities\User;
use yii\base\InvalidValueException;
use yii\base\Object;
use app\modules\users\models\entities\UserData;

class UsersDataManager extends Object
{
    /**
     * @param User $user
     * @param $source
     * @param null $accountId
     * @return \app\modules\users\models\querybuilders\UsersDataAq
     */
    // CHECK
    public function getUserData(User $user, $source, $accountId = null)
    {
        if (empty($user->id)) {
            throw new InvalidValueException('Не передан идентификатор пользователя');
        }
        $query = UserData::find();
        $query->userId($user->id)->source($source);
        if (!empty($accountId)) {
            $query->accountId($accountId);
        }

        return $query;
    }

    public function save(UserData $userData)
    {
        if (!$userData->validate()) {
            return false;
        }

        $userExistsData = $this->getUserData($userData->user, $userData->source, $userData->account_id)->one();
        if (empty($userExistsData)) {
            $queryResult = $userData->insert();
        } else {
            $userExistsData->load($userData->toArray());
            $queryResult = $userExistsData->update();
        }

        return $queryResult;
    }
}