<?php

namespace app\modules\users\components;

use app\modules\users\models\data\EmailPasswordRecoveryData;
use Yii;
use app\modules\users\models\entities\EmailAccount;
use app\modules\users\models\entities\User;
use yii\base\Security;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

// Создание сценариев, констант, удаление стандартных сценариев
// Установка сценариев для всех методов

class EmailAccountsManager extends AbstractAccountsManager
{
    public $passwordRecoverMaxDuration = 86400;

    /**
     * Регистрация аккаунта
     * @param EmailAccount $account
     * @return array|bool
     * @throws ServerErrorHttpException
     */
    public function register(EmailAccount $account)
    {
        return $this->_register($account);
    }

    /**
     * Привязка аккаунта
     * @param EmailAccount $account
     * @param $userData
     * @return array|bool
     * @throws ServerErrorHttpException
     */
    public function assign(EmailAccount $account, $userData)
    {
        return $this->_register($account, $userData);
    }

    /**
     * @param EmailAccount $account
     * @param integer|User|null $userData Данные для создания пользователя, идентификатор или null (автоматическое создание)
     * @return array|bool
     * @throws \Exception
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    protected function _register(EmailAccount $account, $userData = null)
    {
        Yii::trace('Регистрация email-аккаунта');
        $account->setScenario(EmailAccount::SCENARIO_REGISTRATION);

        $usersManager = new UsersManager();
        if (!$account->validate() || $this->authenticate($account)) {
            return false;
        }

        $user = $this->getUserByUserData($userData);

        $account->user_id = $user->id;
        $account->password_hash = Yii::$app->security->generatePasswordHash($account->password);
        $accountCreated = $account->insert();

        /* Если пользователь создан в этом методе и не удалось зарегистрировать аккаунт,
         то удаляем пользователя */
        if (!$accountCreated) {
            // CHECK Проверить проверку новая ли модель, заменить в остальных классах
            if (empty($user->oldAttributes)) {
                $usersManager->delete($user);
            }

            Yii::error('Регистрация не удалась. Ошибка при создании аккаунта');
            throw new ServerErrorHttpException('Не удалось создать аккаунт');
        }

        return ['user' => $userData, 'account' => $account];
    }

    /**
     * @param EmailAccount $accountData
     * @return array|bool|null|\app\models\Entity
     * @throws \yii\base\InvalidConfigException
     */
    public function authenticate(EmailAccount $accountData)
    {
        $authenticateData = clone $accountData;
        $authenticateData->setScenario(EmailAccount::SCENARIO_AUTHENTICATION);
        if (!$authenticateData->validate()) {
            Yii::warning('Аутентификация не удалась. Переданы неверные данные');

            return false;
        }

        $security = new Security;
        Yii::trace('Аутентификация ' . $authenticateData->email);
        $account = EmailAccount::find()->email($authenticateData->email)->one();

        return (!empty($account) && $security->validatePassword($authenticateData->password, $account->password_hash) ? $account : false);
    }

    /**
     * @param EmailAccount $accountData
     * @return bool
     */
    public function authorize(EmailAccount $accountData)
    {
        $accountData->setScenario(EmailAccount::SCENARIO_AUTHORIZATION);
        if (!$accountData->validate()) {
            return false;
        }

        Yii::trace('Авторизация ' . $accountData->email);

        $authManager = new AuthorizationsManager;
        $account = $this->authenticate($accountData);

        return (!empty($account) && !empty($account->user) ? $authManager->authorize($account->user, !$accountData->anotherDevice) : false);
    }

    public function recoverPassword(EmailAccount $recoveryData)
    {
        // Ищет нужный аккаунт, создаёт временного пользоветеля и отправляет на имеил код авторизации
        $recoveryData->setScenario(EmailAccount::SCENARIO_RECOVERY);
        if (!$recoveryData->validate()) {
            return false;
        }
        $tempAccountsManager = new TempAccountsManager;
        $emailAccount = EmailAccount::find()->email($recoveryData->email)->one();
        if (empty($emailAccount)) {
            return false;
        }
        $tempAccountRegister = $tempAccountsManager->assign($emailAccount->user, null, $this->passwordRecoverMaxDuration);
        $tempAccount = $tempAccountRegister['account'];
        $emailMsg = Yii::$app->mailer->compose()
            // TODO Сменить отправителя
            // FUTURE Вынести отправителя в конфиг
            ->setFrom('system@qinfo.com')
            ->setTo($recoveryData->email)
            ->setSubject('Восстановление пароля')
            // :FUTURE Сделать оформление писем
            ->setHtmlBody('<p>Для восстановление пароля перейдите по ссылке ниже</p>
            <p><h1><a href="' . Url::to([
                    '/users/temp-account/authorize',
                    'auth_key' => $tempAccount->authKey,
                    'redirect' => '/users/email-account/password-change',
                ]) . '">Восстановить пароль</a></h1></p>');

        return $emailMsg->send();
    }
}