<?php
/**
 * Created by Dmitriy Baibuhtin
 * Date: 14.01.2015
 */

namespace app\modules\users\models\querybuilders;

use app\modules\users\models\entities\TempAccount;
use Yii;
use yii\base\Security;
use app\components\ActiveQuery;

class TempAccountsAq extends ActiveQuery
{
    public function userId($userId)
    {
        $this->andWhere(['user_id' => $userId]);

        return $this;
    }

    public function authKey($authKey)
    {
        $security = new Security;
        $authKeyHash = $security->hashData($authKey, TempAccount::AUTH_KEY_HASH_KEY);
        $this->andWhere(['auth_key_hash' => $authKeyHash]);

        return $this;
    }
}