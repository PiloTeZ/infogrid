<?php


namespace app\modules\users\models\querybuilders;

use app\components\ActiveQuery;

class UsersDataAq extends ActiveQuery
{
    public function userId($userId)
    {
        $this->andWhere(['user_id' => $userId]);

        return $this;
    }

    public function source($source)
    {
        $this->andWhere(['source' => $source]);

        return $this;
    }

    public function accountId($accountId)
    {
        $this->andWhere(['account_id' => $accountId]);

        return $this;
    }
}