<?php
/**
 * Created by Dmitriy Baibuhtin
 * Date: 14.01.2015
 */

namespace app\modules\users\models\querybuilders;

use app\components\ActiveQuery;
use Yii;

/**
 * @package app\modules\users\models\querybuilders
 */
class ServiceAccountsAq extends ActiveQuery
{
    public function id($id)
    {
        $this->andWhere(['id' => $id]);

        return $this;
    }

    /**
     * @param $userId
     * @return $this
     */
    public function userId($userId)
    {
        $this->andWhere(['user_id' => $userId]);

        return $this;
    }

    /**
     * @param $serviceId
     * @return $this
     */
    public function serviceTextId($serviceId)
    {
        $this->andWhere(['service_text_id' => $serviceId]);

        return $this;
    }

    /**
     * @param $userId
     * @return $this
     */
    public function serviceUserId($userId)
    {
        $this->andWhere(['service_user_id' => $userId]);

        return $this;
    }
}