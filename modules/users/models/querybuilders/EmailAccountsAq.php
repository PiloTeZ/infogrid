<?php
/**
 * Created by Dmitriy Baibuhtin
 * Date: 14.01.2015
 */

namespace app\modules\users\models\querybuilders;

use app\components\ActiveQuery;
use Yii;

class EmailAccountsAq extends ActiveQuery
{
    public function userId($userId)
    {
        $this->andWhere(['user_id' => $userId]);

        return $this;
    }

    /**
     * Поиск по email
     * @param $email
     * @return $this
     */
    public function email($email)
    {
        $this->andWhere(['email' => $email]);

        return $this;
    }
}