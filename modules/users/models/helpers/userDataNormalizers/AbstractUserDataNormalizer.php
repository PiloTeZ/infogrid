<?php


namespace app\modules\users\models\helpers\userDataNormalizers;

use yii\base\Object;

abstract class AbstractUserDataNormalizer extends Object implements UserDataNormalizerInterface
{
    public $userData;

    public function __construct($userData)
    {
        $this->userData = $userData;
    }
}