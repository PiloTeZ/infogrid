<?php


namespace app\modules\users\models\helpers\userDataNormalizers;

interface UserDataNormalizerInterface {
    public function normalize();
}