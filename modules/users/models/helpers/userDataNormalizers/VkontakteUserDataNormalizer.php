<?php


namespace app\modules\users\models\helpers\userDataNormalizers;

use Yii;
use app\modules\users\models\entities\UserData;
use DateTime;

class VkontakteUserDataNormalizer extends AbstractUserDataNormalizer
{
    public function normalize()
    {
        $sex = $this->userData['sex'] - 1;
        $sex = ($sex == -1 ? 2 : $sex);

        $normalizedData = new UserData;
        $normalizedData->nick = $this->userData['first_name'] . ' ' . $this->userData['last_name'];
        $normalizedData->name = $this->userData['first_name'];
        $normalizedData->surname = $this->userData['last_name'];
        $normalizedData->sex = $sex;
        if (!empty($this->userData['bdate'])) {
            $normalizedData->birth_date = Yii::$app->formatter->asTimestamp($this->userData['bdate']);
        }
        $normalizedData->country = (empty($this->userData['country']) ? null : $this->userData['country']);
        $normalizedData->city = (empty($this->userData['city']) ? null : $this->userData['city']);
        $normalizedData->time_zone = (empty($this->userData['timezone']) ? 0 : $this->userData['timezone']);
        $normalizedData->photo = (empty($this->userData['photo']) ? null : (string) $this->userData['photo']);

        return $normalizedData;
    }
}