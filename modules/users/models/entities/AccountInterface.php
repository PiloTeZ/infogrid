<?php


namespace app\modules\users\models\entities;

interface AccountInterface
{
    /**
     * @return array [account => Тип аккаунта, name => Имя аккаунта]
     */
    public function accountLabel();

    public function getUser();
}