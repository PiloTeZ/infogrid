<?php

namespace app\modules\users\models\entities;

use Yii;
use app\modules\users\models\querybuilders\TempAccountsAq;

/**
 * This is the model class for table "users_accounts_temp".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $authorization_time
 * @property integer $registration_time
 * @property integer $expires_time
 * @property string $auth_key_hash
 */
class TempAccount extends AbstractAccount
{
    const SCENARIO_REGISTRATION = 'registration';
    const SCENARIO_AUTHORIZATION = 'authorization';
    const SCENARIO_AUTHENTICATION = 'authentication';
    const AUTH_KEY_HASH_KEY = 'jkrgt6/*&^-=-3kdfg:.-/02934-';
    public $authKey;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_accounts_temp';
    }

    public static function find()
    {
        return new TempAccountsAq(self::className());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Регистрация
            [['auth_key_hash', 'expires_time', 'authorization_time', 'registration_time'], 'required', 'on' => $this::SCENARIO_REGISTRATION],
            [['user_id'], 'required', 'on' => $this::SCENARIO_REGISTRATION],
            // Авторизация/аутентификация
            [['authKey'], 'required', 'on' => [$this::SCENARIO_AUTHORIZATION, $this::SCENARIO_AUTHENTICATION]],
            // Формат полей
            [['user_id', 'expires_time'], 'integer'],
            [['auth_key_hash'], 'string', 'max' => 128],
            [['authKey'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'auth_key_hash' => 'Хэш ключа авторизации',
        ];
    }

    public function scenarios()
    {
        return [
            $this::SCENARIO_REGISTRATION => [],
            $this::SCENARIO_AUTHORIZATION => [],
            $this::SCENARIO_AUTHENTICATION => [],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function accountLabel()
    {
        return [
            'type' => 'Временный',
            'name' => 'будет удалён до ' . Yii::$app->formatter->asDate($this->expires_time, 'eeee dd MMMM')
        ];
    }
}
