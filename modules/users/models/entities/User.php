<?php

namespace app\modules\users\models\entities;

use app\models\Entity;
use app\modules\users\models\querybuilders\UsersAq;
use yii\web\IdentityInterface;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property integer $authorization_time
 * @property integer $registration_time
 * @property string $auth_key
 * @property string $access_token
 */
class User extends Entity implements IdentityInterface
{
    const SCENARIO_CREATION = 'creation';
    const SCENARIO_AUTHORIZATION = 'authorization';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Создание пользователя
            [['authorization_time', 'registration_time'], 'required', 'on' => [$this::SCENARIO_CREATION]],
            // Общие правила
            [['authorization_time', 'registration_time'], 'integer'],
            [['auth_key', 'access_token'], 'string', 'max' => 128]
        ];
    }

    public function scenarios()
    {
        return [
            $this::SCENARIO_CREATION => ['registration_time'],
            $this::SCENARIO_AUTHORIZATION => ['authorization_time'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'authorization_time' => 'Authorization Time',
            'registration_time' => 'Registration Time',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public function getEmailAccounts()
    {
        return $this->hasMany(EmailAccount::className(), ['user_id' => 'id']);
    }

    public function getServiceAccounts()
    {
        return $this->hasMany(ServiceAccount::className(), ['user_id' => 'id']);
    }

    public function getTempAccounts()
    {
        return $this->hasMany(TempAccount::className(), ['user_id' => 'id']);
    }

    public function getUserData()
    {
        return $this->hasMany(UserData::className(), ['user_id' => 'id']);
    }

    public static function find()
    {
        return new UsersAq(self::className());
    }
}
