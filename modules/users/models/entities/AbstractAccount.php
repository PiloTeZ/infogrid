<?php


namespace app\modules\users\models\entities;

use app\models\Entity;
use yii\helpers\Inflector;

abstract class AbstractAccount extends Entity implements AccountInterface
{
    // CHECK
    /**
     * Получение типа аккаунта (temp, email, service...)
     * @return string
     */
    public function accountType()
    {
        $className = $this->className();
        $nameStart = strrpos('\\', $className);
        $nameEnd = stripos('Account', $className);

        return Inflector::camel2id(substr($className, $nameStart, $nameEnd));
    }
}