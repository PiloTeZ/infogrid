<?php

namespace app\modules\users\models\entities;

use app\modules\users\models\querybuilders\EmailAccountsAq;
use Yii;

/**
 * This is the model class for table "users_accounts_email".
 *
 * @property string $email
 * @property string $password_hash
 * @property integer $user_id
 */
class EmailAccount extends AbstractAccount
{
    const SCENARIO_REGISTRATION = 'registration';
    const SCENARIO_AUTHORIZATION = 'authorization';
    const SCENARIO_AUTHENTICATION = 'authentication';
    const SCENARIO_RECOVERY = 'recovery';

    public $password;
    public $anotherDevice = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_accounts_email';
    }

    public static function find()
    {
        return new EmailAccountsAq(self::className());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Общие правила
            [['email', 'password'], 'required'],
            // Регистрация
            ['email', 'unique', 'on' => [$this::SCENARIO_REGISTRATION]],
            // Авторизация
            [['anotherDevice'], 'required', 'on' => [$this::SCENARIO_AUTHORIZATION]],
            // Формат полей
            ['email', 'email'],
            ['password', 'string', 'min' => 6, 'max' => 32],
            ['password_hash', 'string'],
            ['anotherDevice', 'boolean'],
        ];
    }

    public function scenarios()
    {
        return [
            $this::SCENARIO_REGISTRATION => ['email', 'password'],
            $this::SCENARIO_AUTHORIZATION => ['email', 'password', 'anotherDevice'],
            $this::SCENARIO_AUTHENTICATION => ['email', 'password'],
            $this::SCENARIO_RECOVERY => ['email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'password' => 'Пароль',
            'anotherDevice' => 'Чужой компьютер',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function accountLabel()
    {
        return ['type' => 'Email/пароль', 'name' => $this->email];
    }
}
