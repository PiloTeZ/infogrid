<?php

namespace app\modules\users\models\entities;

use app\modules\users\components\ServicesManager;
use Yii;
use app\modules\users\models\querybuilders\ServiceAccountsAq;
use yii\authclient\BaseOAuth;

/**
 * This is the model class for table "users_accounts_service".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $service_text_id
 * @property string $service_user_id
 * @property string $token
 * @property string $token_secret
 * @property integer $token_create_time
 * @property integer $token_expire_duration
 */
class ServiceAccount extends AbstractAccount
{
    const SCENARIO_REGISTRATION = 'registration';
    const SCENARIO_AUTHORIZATION = 'authorization';
    const SCENARIO_TOKEN_UPDATING = 'token_updating';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_accounts_service';
    }

    public static function find()
    {
        return new ServiceAccountsAq(self::className());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        // FUTURE Добавить правило запрещающее сохранение одного из полей токена.
        // Например запрещено сохранять token_expire_duration без token_create_time и token
        return [
            // Формат полей
            [['user_id', 'service_text_id', 'service_user_id'], 'required'],
            [['user_id', 'service_user_id', 'token_create_time', 'token_expire_duration'], 'integer'],
            [['service_text_id'], 'string', 'max' => 32],
            [['token', 'token_secret'], 'string', 'max' => 256],
        ];
    }

    public function scenarios()
    {
// TODO Разобраться в сценариях. Куда какие правила, общие правила, как устанавливаются значения и всё ли обрабатывается
// Что значит объявление полей в сценарии, ведь это вроде только для массового присвоения, а на ручную установку они не влияют.
// Тогда вообще очистить текущие сценарии, ведь они не заливаются через load?
// Может быть объявление сценариев нужно только при массовом присвоенииы
        $commonFields = ['token', 'token_secret', 'token_create_time', 'token_expire_duration'];

        return [
            $this::SCENARIO_REGISTRATION => $commonFields,
            $this::SCENARIO_AUTHORIZATION => $commonFields,
            // CHECK Обновление отдельных полей
            $this::SCENARIO_TOKEN_UPDATING => [],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'service_text_id' => 'Service Text ID',
            'service_user_id' => 'Service User ID',
            'token' => 'Token',
            'token_secret' => 'Token Secret',
            'token_create_time' => 'Token Create Time',
            'token_expire_duration' => 'Token Expire Duration',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserData()
    {
        return UserData::find()->userId($this->user_id)->source(UserData::SOURCE_ACCOUNT_SERVICE)->accountId($this->id);
    }

    public function populateToken($token, $secretToken = null, $createTimestamp, $expireDuration)
    {
        if (empty($token)) {
            return false;
        }

        $this->token = $token;
        $this->token_secret = $secretToken;
        $this->token_create_time = $createTimestamp;
        $this->token_expire_duration = $expireDuration;

        return true;
    }

    public function accountLabel()
    {
        $servicesManager = new ServicesManager;
        $service = $servicesManager->getService($this->service_text_id);

        return ['type' => $service->title, 'name' => $this->userData->nick];
    }
}