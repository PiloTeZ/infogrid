<?php

namespace app\modules\users\models\entities;

use app\models\Entity;
use app\models\EntityInterface;
use app\modules\users\models\querybuilders\UsersDataAq;
use Yii;

/**
 * This is the model class for table "users_data".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $source
 * @property integer $account_id
 * @property string $nick
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $sex
 * @property integer $birth_date
 * @property integer $country
 * @property integer $city
 * @property string $photo
 * @property integer $time_zone
 */
class UserData extends Entity implements EntityInterface
{
    const SCENARIO_CREATION = 'creation';
    const SCENARIO_UPDATING = 'updating';
    const SOURCE_ACCOUNT_SERVICE = 'account_service';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'source', 'account_id', 'nick'], 'required'],
            [['user_id', 'account_id', 'birth_date', 'country', 'city', 'time_zone'], 'integer'],
            [['source'], 'string'],
            [['sex'], 'in', 'range' => [0, 1, 2]],
            [['nick'], 'string', 'max' => 128],
            [['name', 'surname', 'patronymic'], 'string', 'max' => 64],
            [['photo'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'source' => 'Source',
            'account_id' => 'Account ID',
            'nick' => 'Nick',
            'name' => 'Name',
            'surname' => 'Surname',
            'patronymic' => 'Patronymic',
            'sex' => 'Sex',
            'birth_date' => 'Birth Date',
            'country' => 'Country',
            'city' => 'City',
            'photo' => 'Photo',
            'time_zone' => 'Time Zone',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function find()
    {
        return new UsersDataAq(self::className());
    }
}