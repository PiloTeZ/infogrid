<?
use \yii\helpers\Url;

?>

<div class="panel panel-default">
    <div class="panel-heading">Привязанные аккаунты</div>
    <div class="panel-body">
        <ul class="list-group">
            <? foreach ($accounts as $account) { ?>
                <? $accountLabel = $account->accountLabel() ?>
                <li class="list-group-item clearfix">
                    <?= $accountLabel['type'] ?> (<?= $accountLabel['name'] ?>)
                    <div class="pull-right"><a href="<?= Url::to([
                            'users//delete',
                            'account_id' => $account->id,
                            'redirect' => '/users/accounts/list'
                        ]) ?>" class="btn btn-danger">Удалить</a>
                </li>
            <? } ?>
            <?= (empty($accounts) ? '<li class="list-group-item">Аккаунтов нет</li>' : null) ?>
        </ul>
    </div>
</div>
<div class="panel panel-default accounts-add">
    <div class="panel-heading">Добавить аккаунт</div>
    <div class="panel-body">
        <a href="<?= Url::to(['/users/email-account/assign']) ?>"<span
            class="glyphicon glyphicon-envelope pull-left" aria-hidden="true"></span></a>
        <div
            class="pull-left"><?= \yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['/users/service-account/assignService']]) ?></div>
    </div>
</div>