<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<? // FUTURE Вынести эти настройки форм в конфигурацию по умолчанию ?>
<?php $form = ActiveForm::begin([
	'validateOnBlur' => false,
	'validateOnChange' => false,
]); ?>

<?= $form->field($account, 'email') ?>
<?= $form->field($account, 'password')->passwordInput(); ?>
<?= $form->field($account, 'anotherDevice')->checkbox(); ?>

	<div class="form-group">
		<?= Html::submitButton('Войти', ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Забыл пароль', ['/users/email-account/password-recovery']) ?>
	</div>
<?php ActiveForm::end(); ?>