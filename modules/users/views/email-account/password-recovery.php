<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'validateOnBlur' => false,
    'validateOnChange' => false,
]); ?>

<?= $form->field($passwordRecoveryData, 'email')->hint('На этот адрес придёт ссылка для восстановление пароля') ?>

    <div class="form-group">
        <?= Html::submitButton('Восстановить пароль', ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>