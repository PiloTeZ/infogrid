<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
    <?php $form = ActiveForm::begin([
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>

        <?= $form->field($account, 'email') ?>
        <?= $form->field($account, 'password')->passwordInput() ?>

        <div class="form-group">
            <?= Html::submitButton((empty($submitButtonName) ? 'Зарегистрироваться' : $submitButtonName), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
