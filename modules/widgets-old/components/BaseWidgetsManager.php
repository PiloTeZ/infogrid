<?php


namespace app\modules\widgets\components;

use app\modules\widgets\models\entities\BaseWidget;
use Faker\Provider\Base;
use yii\base\Object;
use yii\helpers\Inflector;
use yii\helpers\Json;

class BaseWidgetsManager extends Object
{
    public $widgetsNamespace = '\app\modules\widgets\widgets';
    public $widgetsPath = '@app/modules/widgets/widgets';
    public $baseWidgetName = 'base';

    public function getWidgets()
    {
        return BaseWidget::find()->all();
    }

    /**
     * Получение конфигурации виджета по умолчанию
     * @param BaseWidget $baseWidget
     * @return string
     */
    public function getConfig(BaseWidget $baseWidget)
    {
        $configClass = $this->getConfigClass($baseWidget);
        $config = new $configClass;
        $config->setAttributes((array) Json::decode($baseWidget->config_json));

        return $config;
    }

    public function getWidgetsNamespace(BaseWidget $widget = null)
    {
        return $this->widgetsNamespace . '\\' . (empty($widget) ? $this->baseWidgetName : Inflector::variablize($widget->text_id));
    }

    public function getWidgetsPath(BaseWidget $widget = null)
    {
        return $this->widgetsPath . '/' . (empty($widget) ? $this->baseWidgetName : Inflector::variablize($widget->text_id));
    }

    public function getClass(BaseWidget $widget = null)
    {
        return $this->getWidgetsNamespace($widget) . '\\' . Inflector::id2camel((empty($widget) ? $this->baseWidgetName : $widget->text_id));
    }

    public function getConfigClass(BaseWidget $widget = null)
    {
        // Кастомизированный конфиг
        $configClass = $this->getClass($widget) . 'Config';
        if (!empty($widget) && !class_exists($configClass)) {
            // Конфиг по умолчанию
            $configClass = $this->getConfigClass();
        }

        return $configClass;
    }

    public function getSettingsClass(BaseWidget $widget = null)
    {
        return $this->getClass($widget) . 'Settings';
    }

    public function getAssetClass(BaseWidget $widget = null)
    {
        return $this->getWidgetsNamespace($widget) . '\\assets\\' . $widget->camelTextId . 'Asset';
    }

    public function getAssetDependsClass(BaseWidget $widget = null)
    {
        return $this->getAssetClass($widget) . 'Depends';
    }
}