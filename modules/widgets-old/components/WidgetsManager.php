<?php


namespace app\modules\widgets\components;

use Yii;
use app\modules\widgets\models\entities\BaseWidget;
use yii\base\Object;
use app\modules\widgets\models\entities\Widget;
use app\modules\users\models\entities\User;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\widgets\ActiveForm;

class WidgetsManager extends Object
{
    /**
     * Получение виджетов
     * @param User $user
     * @param bool $order
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getAll(User $user, $order = true)
    {
        // TODO IMPORTANT В будущем, если вместо $user передан true, возвращать виджеты для всех пользователей. Не использовать false,
        // так как система может посчитать к примеру отсутствие идентификатора у гостя
        // как запрос по всем пользователям, использовать строгое сравнение
        $query = Widget::find()->userId($user->id);
        if ($order) {
            $query->addOrderBy(['order' => SORT_ASC]);
        }
        return $query->all();
    }

    /**
     * @param $widgetId
     * @return Widget
     */
    public function getOne($widgetId)
    {
        // TODO Ограничение по пользователю
        return Widget::findOne((int)$widgetId);
    }

    /**
     * Получение конфигурации виджета
     * @param Widget $widget
     * @return string
     */
    public function getConfig(Widget $widget)
    {
        $baseWidgetsManager = new BaseWidgetsManager;
        $config = $baseWidgetsManager->getConfig($widget->baseWidget);
        $config->setAttributes((array)Json::decode($widget->config_json));

        return $config;
    }

    /**
     * Получение класса настроек (стандартный класс настроек или кастомизированный)
     * @param Widget $widget
     * @return string
     */
    public function getSettingsClass(Widget $widget)
    {
        $baseWidgetsManager = new BaseWidgetsManager;
        $settingsClass = $baseWidgetsManager->getSettingsClass($widget->baseWidget);

        return (class_exists($settingsClass) ? $settingsClass : $baseWidgetsManager->getSettingsClass());
    }

    public function getAdditionalSettingsView(Widget $widget)
    {
        $baseWidgetsManager = new BaseWidgetsManager;
        $settingsViewPath = $baseWidgetsManager->getWidgetsPath($widget->baseWidget) . '/views/settings.php';
        return (file_exists(Yii::getAlias($settingsViewPath)) ? $settingsViewPath : null);
    }

    /**
     * @param $baseWidget
     * @param User $user
     * @return object|bool
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Exception
     */
    public function add($baseWidget, User $user)
    {
        if (is_numeric($baseWidget)) {
            // TODO Заменить на правильное получение
            $baseWidget = BaseWidget::findOne($baseWidget);
        } else {
            throw new ServerErrorHttpException('Переданы неверные аргументы');
        }

        if (empty($baseWidget)) {
            throw new NotFoundHttpException('Виджет не найден');
        }

        $lastWidget = $this->getLastWidget();
        $widget = new Widget;
        $widget->user_id = $user->id;
        $widget->base_widget_id = $baseWidget->id;
        $widget->config_json = $baseWidget->config_json;
        $widget->order = ($lastWidget ? $lastWidget['order'] : 0) + 1;
        return ($widget->insert() ? $widget : false);
    }

    /**
     * @param $widget
     * @return false|int
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function delete($widget)
    {
        if (is_numeric($widget)) {
            $widget = $this->getOne($widget);
        }

        if (!$widget instanceof Widget) {
            throw new NotFoundHttpException('Виджет не найден');
        }

        return $widget->delete();
    }

    /**
     * @param $widgetId
     * @param array $settings
     * @return bool
     */
    public function saveSettings($widgetId, array $settings)
    {
        $widget = $this->getOne($widgetId);
        $widgetConfig = $widget->config;
        $widgetConfig->setAttributes($settings);
        if ($widgetConfig->validate()) {
            $widget->config_json = Json::encode($widgetConfig->toArray());

            return $widget->save();
        } else {
            return false;
        }
    }

    // Сортировка при выводе
    // Добавление порядкового номера при создании
    /**
     * Сохранение порядка виджетов
     * @param array $widgetsList Массив вида [index => widgetId, index2 => widgetId...]
     */
    public function saveOrder(array $widgetsList)
    {
        // TODO Оптимизировать запрос, проверить безопаснсоть
        // TODO Указание пользователя (не обязательно, если не указан, использовать текущего)
        $db = Yii::$app->db;

        foreach ($widgetsList as $index => $widgetId) {
            $db
                ->createCommand()
                ->update(Widget::tableName(), ['order' => $index], ['id' => intval($widgetId), 'user_id' => Yii::$app->user->id])
                ->execute();
        }
    }

    /**
     * Получение виджета с наибольшим порядковым номером
     */
    public function getLastWidget()
    {
        // TODO Сделать возвращение модели вместо массива + не забыть заменить в местах, где используется этот метод
        // TODO Указание ID пользователя
        return (new \yii\db\Query())
            ->from(Widget::tableName())
            ->where(['user_id' => Yii::$app->user->id])
            ->orderBy(['order' => SORT_DESC])
            ->one();
    }
}