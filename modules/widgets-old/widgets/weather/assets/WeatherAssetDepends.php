<?php
namespace app\modules\widgets\widgets\weather\assets;

use yii\web\AssetBundle;

/** Необязательный класс */
class WeatherAssetDepends extends AssetBundle
{
    public $depends = [
        '\yii\web\JqueryAsset',    ];
}