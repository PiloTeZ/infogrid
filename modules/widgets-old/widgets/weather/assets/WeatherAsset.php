<?php
namespace app\modules\widgets\widgets\weather\assets;

use yii\web\AssetBundle;

/** Необязательный класс */
class WeatherAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/widgets/widgets/Weather/assets';

    public $css = [
        'css/styles.css',
    ];

    public $js = [
        'js/functions.js', 'js/scripts.js',    ];
}