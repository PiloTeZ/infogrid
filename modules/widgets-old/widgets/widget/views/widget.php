<div id="<?= $widgetEntity->textId ?>"
     class="widget" data-widget-width="<?= $widgetConfig->width ?>" data-widget-height="<?= $widgetConfig->height ?>"
     data-widget-id="<?= $widgetEntity->id ?>">
    <div class="header clearfix">
        <!-- TODO Перенести в CSS .notes-widget-icon -->
        <div class="icon"><span><img src="/icons/widgets/<?= $widgetEntity->baseWidget->text_id ?>.png" alt=""/></span></div>
        <div class="title"><span><?= $widgetConfig->name ?></span></div>
        <ul class="menu">
            <li class="actions">
                <a href="#" class="<?= $widgetEntity->textId ?>-settings-button"></a>
                <!-- <ul>
                    <li>Настройки</li>
                    <li>Удалить</li>
                </ul> -->
            </li>
        </ul>
    </div>
    <div class="content">
        <?= $widgetClass::widget(['widgetEntity' => $widgetEntity]) ?>
    </div>

    <?= \app\modules\widgets\widgets\base\Settings::widget(['widgetEntity' => $widgetEntity]) ?>
</div>