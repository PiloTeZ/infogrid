<?php
namespace app\modules\widgets\widgets\notes\assets;

use yii\web\AssetBundle;

/** Необязательный класс */
class WidgetAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/widgets/widgets/widget/assets';

    public $js = [
        'js/functions.js',
        'js/scripts.js',
    ];

    public $depends = [
    ];
}