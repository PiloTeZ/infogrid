<?php


namespace app\modules\widgets\widgets\widget;

use app\modules\widgets\components\BaseWidgetsManager;
use app\modules\widgets\components\WidgetsManager;
use app\modules\widgets\widgets\base\AbstractBase;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;

class Widget extends \yii\base\Widget
{
    /** @var \app\modules\widgets\models\entities\Widget Виджет для запуска */
    public $widgetEntity = null;
    public $widgetId = null;

    /** @var AbstractBase */
    protected $widgetClass;

    public function init()
    {
        if (!empty($this->widgetId)) {
            // TODO Заменить на правильное получение виджетов через компонет или как там правильно и проверить везде подобную проблему
            $this->widgetEntity = \app\modules\widgets\models\entities\Widget::findOne((int) $this->widgetId);
        }

        if (empty($this->widgetEntity)) {
            throw new NotFoundHttpException('Сущность виджета не найдена');
        }

        $baseWidgetsManager = new BaseWidgetsManager;
        $this->widgetClass = $baseWidgetsManager->getClass($this->widgetEntity->baseWidget);
        if (!class_exists($this->widgetClass)) {
            throw new NotFoundHttpException('Класс виджета не найден');
        }
    }

    public function run()
    {
        $widgetsManager = new WidgetsManager;

        return $this->render('widget', [
            'widgetEntity' => $this->widgetEntity,
            'widgetClass' => $this->widgetClass,
            'widgetConfig' => $widgetsManager->getConfig($this->widgetEntity),
        ]);
    }
}