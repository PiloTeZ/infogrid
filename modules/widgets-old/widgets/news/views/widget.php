<div class="feeds">
    <div class="feed-select">
        <select ng-model="feedIndex" id="<?= $widgetTextId ?>-feed-select">
            <option ng-repeat="(index, feed) in feeds" value="{{index}}">{{feed.title}}</option>
        </select>
    </div>
    <div class="feed">
        <div class="item" ng-repeat="feed in feeds[feedIndex].content">
            <div class="title"><a ng-href="{{feed.link}}">{{feed.title}}</a></div>
            <div class="content" style="white-space: pre-line"><p>{{feed.content}}</p></div>
        </div>
    </div>
</div>