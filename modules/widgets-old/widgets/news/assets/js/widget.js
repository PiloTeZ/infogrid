WidgetsModule.controller('NewsWidgetController', ['$scope', '$http', function ($scope, $http) {
    /** array Список источников */
    $scope.feeds = [];
    /** object Контент фидов */
    $scope.feedIndex = null;

    $scope.init = function (params) {
        $scope.setFeeds(params.feeds);
        $scope.setFeed(0);
        $scope.trackFeedChange();
    };

    /**
     * @param feeds array {name: string, url: string}
     */
    $scope.setFeeds = function (feeds) {
        $scope.feeds = feeds;
    };
    $scope.setFeed = function (feedIndex) {
        $scope.feedIndex = feedIndex;
    };
    $scope.updateFeed = function (feedIndex) {
        var feed = $scope._getFeed(feedIndex);
        if (typeof feed.content == 'undefined') {
            $http.jsonp('https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=100&scoring=h&callback=JSON_CALLBACK&q=' + feed.url)
                .success(function (data) {
                    $scope._updateFeed(feedIndex, data.responseData.feed);
                });
        }
    };
    $scope.trackFeedChange = function () {
        $scope.$watch('feedIndex', function (newValue) {
            $scope.updateFeed(newValue);
        });
    };
    $scope._updateFeed = function (feedIndex, feedData) {
        var entries = [];
        angular.forEach(feedData.entries, function (entry) {
            // TODO Замену долбанных nbsp и специальных символов HTML &...;
            entry.content = entry.content
                // Конвертация кавчек
                .replace(/&quot;/g, '"')
                // Полное удаление ссылок, удаление дополнительных символов
                // (&#\d{1,5}) Вместо удаления дополнительных символов, сделано полное удаление HTML сущностей
                .replace(/(<a.*?>.*?<\/a>)|(\t)/g, '')
                // Форматирование переносов строк
                .replace(/((<br\/?>)|(\n))+/gm, '\n')
                // Удаление тегов оставляя содержимое
                .replace(/<.+?>/gm, '')
                .trim();
            entries.push(entry);
        });
        $scope.feeds[feedIndex].content = entries;
    };

    $scope._getFeed = function (feedIndex) {
        return $scope.feeds[feedIndex];
    };
}]);


/*
 $scope.feeds = [];
 $scope.feedsContent = [];
 $scope.feed = {
 index: 0,
 content: []
 };

 $scope.init = function (params) {
 $scope.feeds = params.feeds;
 $scope.$watch('feed.index', function (newVal, oldVal) {
 $scope.changeFeed(newVal);
 });
 };
 $scope.changeFeed = function (index) {
 $scope.updateContent(index);
 };
 $scope.updateContent = function (index) {
 if (index === false) {
 return false;
 }
 // TODO Учитывание отсутствия текущего индекса
 if ($scope.feeds[index].content == null) {
 // Обновление данных
 console.log('Обновление фида');
 $scope.feeds[index].content = [];
 setTimeout(function () {
 $scope.feeds[index].content = [
 {
 title: 'В Австрии телёнок перепрыгнул через тихий океан',
 date: '02.04.2015',
 text: 'Очень подробный текст об этой замечательной новости'
 },
 {
 title: 'В Австрии телёнок перепрыгнул через тихий океан',
 date: '02.04.2015',
 text: 'Очень подробный текст об этой замечательной новости'
 }
 ];
 $scope.feed.content = $scope.feeds[index].content;
 $scope.feed.lastUpdate = new Date.now();
 // TODO Автообновление контента каждые 3 минуты
 $scope.$apply();
 console.log('Фид обновлен');
 }, 1000);
 } else {
 // Кешированные данные
 console.log('Фид взят из кеша');
 $scope.feed.content = $scope.feeds[index].content;
 }
 };
 */