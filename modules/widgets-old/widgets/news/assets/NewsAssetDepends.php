<?php
namespace app\modules\widgets\widgets\news\assets;

use yii\web\AssetBundle;

/** Необязательный класс */
class NewsAssetDepends extends AssetBundle
{
    public $depends = [
        '\yii\web\JqueryAsset',    ];
}