<?php
namespace app\modules\widgets\widgets\news\assets;

use yii\web\AssetBundle;

/** Необязательный класс */
class NewsAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/widgets/widgets/News/assets';

    public $css = [
        'css/styles.css',
    ];

    public $js = [
        'js/widget.js', 'js/settings.js'
    ];
}