<?php
namespace app\modules\widgets\widgets\news;

use app\modules\widgets\widgets\base\BaseConfig;
use yii\helpers\ArrayHelper;

/** Необязательный класс */
class NewsConfig extends BaseConfig
{
    public $feeds = array();

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['feeds', 'validateFeeds']
        ]);
    }

    public function validateFeeds($attribute, $params)
    {
        if (!is_array($this->feeds)) {
            $this->addError($attribute, 'Список фидов должен быть массивом');
        }
    }
}