<?php
namespace app\modules\widgets\widgets\news;

use app\modules\widgets\widgets\base\AbstractBase;

class News extends AbstractBase
{
    protected $useAngularJs = true;

    public function init()
    {
        parent::init();
        // Код
    }

    public function run()
    {
        // Код
        $jsParams = [];
        $jsParams['feeds'] = $this->widgetConfig->feeds;
        return parent::run([], $jsParams);
    }
}