<div id="widgets-catalog-modal" title="Каталог виджетов" style="display: none;">
    <!-- Вынести в отдельный виджет -->
    <div class="widgets-catalog clearfix">
        <? foreach ($baseWidgets as $widget) { ?>
            <? $widgetConfig = $widget->getConfig() ?>
            <div class="widget">
                <div class="image"><span><img src="/images/widgets/<?= $widget->text_id?>.png" alt="<?= $widgetConfig->name ?>"/></span></div>
                <div class="info">
                    <div class="name"><span><?= $widgetConfig->name ?></span></div>
                    <div class="details">
                        <? if (!empty($widget->description)) { ?>
                            <div class="description"><span><?= $widget->description ?></span></div>
                        <? } ?>
                        <div class="buttons">
                            <button class="add-widget-button" data-widget-id="<?= $widget->id ?>">Добавить виджет</button>
                        </div>
                    </div>
                </div>
            </div>
        <? } ?>
    </div>
    <!-- END Вынести в отдельный виджет -->
</div>
