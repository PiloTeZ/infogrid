<?php


namespace app\modules\widgets\widgets\widgetsCatalogModal;

use app\modules\widgets\components\BaseWidgetsManager;
use app\modules\widgets\widgets\widgetsCatalogModal\assets\WidgetsCatalogModalAsset;
use yii\base\Widget;

class WidgetsCatalogModal extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        WidgetsCatalogModalAsset::register($this->view);
        $baseWidgetsManager = new BaseWidgetsManager;
        $widgets = $baseWidgetsManager->getWidgets();

        return $this->render('widgets', ['baseWidgets' => $widgets]);
    }
}