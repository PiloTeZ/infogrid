$(document).ready(function () {
    // Сделать обёртку с настройками по умолчанию
    var widgetsCatalogModal = $('#widgets-catalog-modal').dialog({
        modal: true,
        autoOpen: false,
        dialogClass: 'widgets-catalog-modal widgets-catalog',
        resizable: false,
        closeText: 'Закрыть',
        position: {my: "center top", at: "center top+90"}
    });

    $('.widgets-catalog-modal-button').on('click', function () {
        widgetsCatalogModal.dialog('open');
        return false;
    });
    $('.widgets-catalog .add-widget-button').on('click', function () {
        widgetsCatalogModal.dialog('close');
        var widgetId = $(this).attr('data-widget-id');
        $(document).trigger('add-widget', {widgetId: widgetId});
        return false;
    });
});