<?php


namespace app\modules\widgets\widgets\widgetsCatalogModal\assets;

use yii\web\AssetBundle;

class WidgetsCatalogModalAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/widgets/widgets/widgetsCatalogModal/assets';

    public $js = [
      'js/scripts.js'
    ];

    public $depends = [
        'app\assets\CommonAsset',
    ];
}