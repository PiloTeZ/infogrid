<?php
namespace app\modules\widgets\widgets\notes;

use app\modules\notes\entities\Note;
use Yii;
use app\modules\widgets\widgets\base\AbstractBase;

// TODO position absolute и увелечение размеров при фокусе на форме ввода для удобства

class Notes extends AbstractBase
{
    protected $useAngularJs = true;

    public function init()
    {
        parent::init();
        // Код
    }

    public function run()
    {
        $note = Note::findOne($this->widgetConfig->noteId);
        if (empty($note)) {
            Yii::error('Не найдена записка с ID ' . $this->widgetConfig->noteId);
        }
        $params = ['note' => $note];

        return parent::run($params, $params);
    }
}