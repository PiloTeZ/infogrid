<?php
namespace app\modules\widgets\widgets\notes;

use app\modules\widgets\widgets\base\BaseConfig;
use yii\helpers\ArrayHelper;

/** Необязательный класс */
class NotesConfig extends BaseConfig
{
    public $noteId;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['noteId', 'number']
        ]);
    }
}