WidgetsModule.service('NotesWidgetService', function ($resource) {
    // TODO Заменить URL, вынести URL API в глобальную переменную Angular
    this.apiUrl = 'http://briefinfo.net/api/notes-widget-rest/';
    this.api = $resource(this.apiUrl + ':noteId',
        {noteId: '@noteId'},
        {'update': {method: 'PUT'}}
    );

    this.save = function (note) {
        return this.api.update({noteId: note.id}, note);
    };
});