function NotesWidgetController($scope, $interval, NotesWidgetService) {
    $scope.widgetEntity = {};
    $scope.widgetBase = {};

    /** @var integer Время последнего редактирования */
    $scope.lastChangeTime = 0;
    /** @var integer Длительность задержки перед сохранением */
    $scope.saveDelay = 1000;
    /** @var object Интервал отслеживания статуса печатания для определения момента сохранения */
    $scope.changesTrackInterval = null;

    $scope.init = function (params) {
        $scope.note = params.note;
    };

    /**
     * Созранение записки
     */
    $scope.onChange = function () {
        $scope.lastChangeTime = new Date().getTime();

        if (!$scope.changesTrackInterval) {
            $scope.changesTrackInterval = $interval(function () {
                // Сохранять через $scope.saveDelay секунд после последнего изменения
                if ((new Date().getTime() - $scope.lastChangeTime) > $scope.saveDelay) {
                    console.log('Сохранение записки');

                    NotesWidgetService.save($scope.note);

                    $interval.cancel($scope.changesTrackInterval);
                    $scope.changesTrackInterval = null;
                } else {
                    console.log('Ожидание ввода');
                }
            }, $scope.saveDelay);
        }
    };
}

WidgetsModule.controller('NotesWidgetController', ['$scope', '$interval', 'NotesWidgetService', NotesWidgetController]);