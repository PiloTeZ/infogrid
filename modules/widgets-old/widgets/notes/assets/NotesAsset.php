<?php
namespace app\modules\widgets\widgets\notes\assets;

use yii\web\AssetBundle;

/** Необязательный класс */
class NotesAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/widgets/widgets/Notes/assets';

    public $css = [
        'css/styles.css'
    ];

    public $js = [
        'js/notes-widget-service.js',
        'js/notes-widget-controller.js',
    ];
}