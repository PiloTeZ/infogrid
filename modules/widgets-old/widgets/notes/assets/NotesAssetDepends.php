<?php


namespace app\modules\widgets\widgets\notes\assets;

use yii\web\AssetBundle;

class NotesAssetDepends extends AssetBundle
{
    public $depends = [
        '\app\modules\widgets\widgets\base\assets\BaseAngularJsAsset'
    ];
}