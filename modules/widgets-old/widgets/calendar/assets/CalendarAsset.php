<?php
namespace app\modules\widgets\widgets\calendar\assets;

use yii\web\AssetBundle;

/** Необязательный класс */
class CalendarAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/widgets/widgets/Calendar/assets';

    public $css = [
        'css/styles.css',
    ];

    public $js = [
        'js/functions.js',
        'js/scripts.js',
    ];
}