<?php


namespace app\modules\widgets\widgets\calendar\assets;

use yii\web\AssetBundle;

class CalendarAssetDepends extends AssetBundle
{
    public $depends = [
        '\yii\web\JqueryAsset',
        '\app\assets\JqueryUiAsset',
    ];
}