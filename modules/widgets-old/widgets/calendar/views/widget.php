<?
$calendarId = $widgetEntity->textId . '-calendar';
?>

    <div class="calendar" id="<?= $calendarId ?>"></div>

<?
$this->registerJs(<<< JS
        $('#{$calendarId}').datepicker({
            showOtherMonths: true
        });
JS
);