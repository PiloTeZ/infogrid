<?php

namespace app\modules\widgets\widgets\base;

use app\modules\widgets\components\BaseWidgetsManager;
use app\modules\widgets\components\WidgetsManager;
use yii\base\InvalidParamException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Класс наследуется всеми виджетами как основа
 */
abstract class AbstractBase extends Widget
{
    /** @var \app\modules\widgets\models\entities\Widget Виджет для запуска */
    public $widgetEntity;
    public $widgetConfig;
    public $viewTemplate = 'widget';
    protected $useAngularJs = false;

    public function init()
    {
        if (empty($this->widgetEntity)) {
            throw new InvalidParamException('Сущность виджета не найдена');
        }

        // Подключение ресурсов
        $baseWidgetsManager = new BaseWidgetsManager;
        $widgetAsset = $baseWidgetsManager->getAssetClass($this->widgetEntity->baseWidget);
        $widgetAsset::register($this->view);

        // Чтение конфигурации
        $widgetsManager = new WidgetsManager;
        $this->widgetConfig = $widgetsManager->getConfig($this->widgetEntity);
    }

    /**
     * @param array $params
     * @param array $jsParams
     * @return string
     */
    public function run(array $params = [], array $jsParams = [])
    {
        $widgetsManager = new WidgetsManager;
        $widgetConfig = $widgetsManager->getConfig($this->widgetEntity);

        // Общие параметры для виджета, врапперов и JS-виджетов
        $commonParams = [
            'widgetEntity' => $this->widgetEntity,
            'widgetConfig' => $widgetConfig,
            'baseWidget' => $this->widgetEntity->baseWidget,
            'widgetTextId' => $this->widgetEntity->baseWidget->text_id . '-' . $this->widgetEntity->id
        ];

        // Кастомизированные параметры
        $widgetParams = ArrayHelper::merge($commonParams, $params);

        // Кастомизированные JS параметры
        $widgetJsParams = ArrayHelper::merge($commonParams, $jsParams);

        ob_start();
        $this->beforeRender($commonParams, $widgetParams, $widgetJsParams);
        echo $this->render($this->viewTemplate, $widgetParams);
        $this->afterRender();

        return ob_get_clean();
    }

    public function beforeRender($commonParams, $widgetParams, $widgetJsParams)
    {
        BaseWrapper::begin($commonParams);
        if ($this->useAngularJs) {
            BaseAngularJsWrapper::begin(ArrayHelper::merge($commonParams, ['jsParams' => $widgetJsParams]));
        }
    }

    public function afterRender()
    {
        if ($this->useAngularJs) {
            BaseAngularJsWrapper::end();
        }
        BaseWrapper::end();
    }
}