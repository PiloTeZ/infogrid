<?php


namespace app\modules\widgets\widgets\base;

use yii\base\Model;

/** Базовая конфигурация виджета, а так же класс конфигурации по умолчанию */
class BaseConfig extends Model
{
    public $name = 'Название не заполнено';
    public $width = 1;
    public $height = 1;

    public function rules()
    {
        return [
            [['name', 'width', 'height'], 'required'],
            [['name'], 'string', 'max' => 64],
            [['width', 'height'], 'number', 'min' => 1, 'max' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'width' => 'Ширина',
            'height' => 'Высота',
        ];
    }

    public function formName()
    {
        return 'Config';
    }
}