<?php


namespace app\modules\widgets\widgets\base;

use yii\base\InvalidParamException;
use yii\base\Widget;

class BaseWrapper extends Widget
{
    public $jsParams;
    public $widgetEntity;
    public $widgetConfig;
    public $baseWidget;
    public $widgetTextId;

    public function init()
    {
        if (empty($this->widgetEntity)) {
            throw new InvalidParamException('Не передана сущность виджета');
        }
        ob_start();
    }

    public function run()
    {
        $content = ob_get_clean();

        return $this->render('base', ['widgetEntity' => $this->widgetEntity, 'content' => $content]);
    }
}