<?php


namespace app\modules\widgets\widgets\base;

use app\modules\widgets\widgets\base\assets\BaseAngularJsAsset;
use yii\base\Model;
use yii\base\Widget;
use yii\helpers\Json;

class BaseAngularJsWrapper extends Widget
{
    // TODO Разобраться с передачей WidgetEntity.
    public $jsParams;
    public $widgetEntity;
    public $widgetConfig;
    public $baseWidget;
    public $widgetTextId;

    public function init()
    {
        BaseAngularJsAsset::register($this->view);
        ob_start();
    }

    public function run()
    {
        $content = ob_get_clean();

        $controllerJsonParams = $this->convertParamsToJson($this->jsParams);

        return $this->render('angular-js', ['baseWidget' => $this->baseWidget, 'controllerJsonParams' => $controllerJsonParams, 'content' => $content]);
    }

    public function convertParamsToJson(array $params)
    {
        /** @var array $paramsAsArray Конвертация моделей в массивы */
        $paramsAsArray = [];

        foreach ($params as $key => $param) {
            $paramsAsArray[$key] = ($param instanceof Model ? $param->toArray() : $param);
        }

        return Json::encode($paramsAsArray);
    }
}