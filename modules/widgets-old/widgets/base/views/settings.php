<?
use \yii\widgets\ActiveForm;

// TODO JS валидация
/** @var $widgetEntity \yii\base\Model */
/** @var $widgetConfig \yii\base\Model */
?>
    <div id="<?= $widgetEntity->textId ?>-settings" style="display: none;">
        <?php $form = ActiveForm::begin([
            'validateOnBlur' => false,
            'validateOnChange' => false,
        ]); ?>

        <?= $form->field($widgetConfig, 'name') ?>
        <?= $form->field($widgetConfig, 'width')->textInput(['type' => 'number']) ?>
        <?= $form->field($widgetConfig, 'height')->textInput(['type' => 'number']) ?>

        <? if (!empty($additionalSettingsView)) { ?>
            <?= $this->render($additionalSettingsView, array_merge(['settingsForm' => $form], $_params_)) ?>
        <? } ?>

        <?php ActiveForm::end(); ?>
    </div>

<? $this->registerJs(<<< JS
var widgetSettingsElement = $('#{$widgetEntity->textId}-settings');
        var widgetSettingsDialog = widgetSettingsElement.dialog({
        dialogClass: 'widget-settings-modal',
        resizable: false,
        closeText: 'Закрыть',
        position: { my: "center top", at: "center top+90" },
        // TODO Важно добавить этот пункт в общую функцию вызова дилога
        closeOnEscape: true,
        title: 'Настройки',
        modal: true,
            buttons: [
                {
                    text: "Сохранить",
                    click: function () {
                        $(this).dialog("close");
                        $(document).trigger('save-widget-settings', {
                        widgetId: '{$widgetEntity->id}',
                        settings: $(this).find('form').serializeObject().Config
                        });
                    }
                },
                {
                    'class': 'grey-button',
                    text: "Отменить",
                    click: function () {
                        $(this).dialog("close");
                    }
                },
                {
                    'class': 'delete-widget-button grey-button',
                    text: "Удалить виджет",
                    click: function (event, element) {
                        // TODO Оптимизировать
                        $(this).dialog("close");
                        $(document).trigger('delete-widget', {widgetId: '{$widgetEntity->id}'});
                    }
                }
            ],
            autoOpen: false
        });

        $('.{$widgetEntity->textId}-settings-button').on('click', function() {
            $('#{$widgetEntity->textId}-settings').dialog( "open" );
        });
JS
);
// Доработать .названия кнопок
// Заменить идентификатор на класс, что бы не засорять память
// Возможно перенести отслеживание нажатия на кнопки из скриптов в api
// TODO Оптимизировать и прорефакторить этот участок кода