<?php


namespace app\modules\widgets\widgets\base;

use app\modules\widgets\components\WidgetsManager;
use yii\base\InvalidParamException;
use yii\base\Widget;

/** Виджет настроек виджета по умолчанию */
class BaseSettings extends Widget
{
    public $widgetEntity;
    public $widgetConfig;

    public function init()
    {
        if (empty($this->widgetEntity)) {
            throw new InvalidParamException('Не передана сущность виджета');
        }

        $widgetsManager = new WidgetsManager;
        $this->widgetConfig = $widgetsManager->getConfig($this->widgetEntity);
    }

    public function run()
    {
        $widgetsManager = new WidgetsManager;
        $additionalSettingsView = $widgetsManager->getAdditionalSettingsView($this->widgetEntity);
        $additionalSettingsJs = $widgetsManager->getAdditionalSettingsView($this->widgetEntity);
        // TODO Подключение JS настроек
        return $this->render('settings', [
            'widgetEntity' => $this->widgetEntity,
            'widgetConfig' => $this->widgetConfig,
            'additionalSettingsView' => $additionalSettingsView,
        ]);
    }
}