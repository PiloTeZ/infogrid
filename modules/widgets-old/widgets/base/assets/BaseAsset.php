<?php


namespace app\modules\widgets\widgets\base\assets;

use yii\web\AssetBundle;

class BaseAsset extends AssetBundle
{
    public $depends = [
        'app\assets\AngularJsAsset',
    ];
}