<?php


namespace app\modules\widgets\widgets\base\assets;

use yii\web\AssetBundle;

class BaseAngularJsAsset extends AssetBundle
{
    public $depends = [
        'app\assets\AngularJsModuleAsset',
    ];
}