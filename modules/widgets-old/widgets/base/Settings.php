<?php


namespace app\modules\widgets\widgets\base;

use app\modules\widgets\components\WidgetsManager;
use yii\base\InvalidParamException;
use yii\base\Widget;

class Settings extends Widget
{
    public $widgetEntity;

    public function init()
    {
        if (empty($this->widgetEntity)) {
            throw new InvalidParamException('Не передана сущность виджета');
        }
    }

    public function run()
    {
        $widgetsManager = new WidgetsManager;
        $widgetSettingsClass = $widgetsManager->getSettingsClass($this->widgetEntity);

        return $widgetSettingsClass::widget(['widgetEntity' => $this->widgetEntity]);
    }
}