<?php

namespace app\modules\widgets;

class Widgets extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\widgets\controllers';

    public function init()
    {
        parent::init();
        $this->layout = 'inner';
        $this->layoutPath = '@app/views/layouts';
    }
}
