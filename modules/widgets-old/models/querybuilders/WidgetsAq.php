<?php
/**
 * Created by Dmitriy Baibuhtin
 * Date: 14.01.2015
 */

namespace app\modules\widgets\models\querybuilders;

use yii\db\ActiveQuery;

class WidgetsAq extends ActiveQuery
{
    /**
     * @param $userId
     * @return $this
     */
    public function userId($userId)
    {
        $this->andWhere(['user_id' => $userId]);

        return $this;
    }
}