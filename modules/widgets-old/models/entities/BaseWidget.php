<?php

namespace app\modules\widgets\models\entities;

use app\modules\widgets\components\BaseWidgetsManager;
use app\modules\widgets\models\querybuilders\BaseWidgetsAq;
use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "widgets_base".
 *
 * @property integer $id
 * @property string $text_id
 * @property string $camelTextId
 * @property string $description
 * @property string $config_json
 */
class BaseWidget extends \yii\db\ActiveRecord
{
    protected $_config;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widgets_base';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text_id', 'name'], 'required'],
            [['text_id'], 'string', 'max' => 32],
            [['description'], 'string', 'max' => 256],
            [['config_json'], 'string', 'max' => 1024],
            [['text_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_id' => 'Text ID',
            'description' => 'Description',
            'config_json' => 'Config Json',
        ];
    }

    public static function find()
    {
        return new BaseWidgetsAq(self::className());
    }

    public function getWidgets()
    {
        return $this->hasMany(Widget::className(), ['base_widget_id' => 'id']);
    }

    public function getCamelTextId()
    {
        return Inflector::id2camel($this->text_id);
    }

    public function getConfig()
    {
        if (empty($this->config)) {
            $widgetsManager = new BaseWidgetsManager;
            $this->_config = $widgetsManager->getConfig($this);
        }

        return $this->_config;
    }
}
