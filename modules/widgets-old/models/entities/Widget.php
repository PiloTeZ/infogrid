<?php

namespace app\modules\widgets\models\entities;

use app\modules\widgets\components\WidgetsManager;
use Yii;
use app\modules\widgets\models\querybuilders\WidgetsAq;

/**
 * This is the model class for table "widgets".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $base_widget_id
 * @property string $config_json
 * @property object $config
 * @property integer $order
 */
class Widget extends \yii\db\ActiveRecord
{
    protected $_config;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widgets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'base_widget_id'], 'required'],
            [['user_id', 'base_widget_id', 'order'], 'integer'],
            [['config_json'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'base_widget_id' => 'Widget Base ID',
            'config_json' => 'Config Json',
            'order' => 'Порядковый номер',
        ];
    }

    public static function find()
    {
        return new WidgetsAq(self::className());
    }

    public function getBaseWidget()
    {
        return $this->hasOne(BaseWidget::className(), ['id' => 'base_widget_id']);
    }

    /**
     * Уникальный идентификатор (текстовый идентификатор + ид виджета)
     */
    public function getTextId()
    {
        return $this->baseWidget->text_id . '-widget-' . $this->id;
    }

    public function getConfig()
    {
        if (empty($this->_config)) {
            $widgetsManager = new WidgetsManager;
            $this->_config = $widgetsManager->getConfig($this);
        }

        return $this->_config;
    }
}