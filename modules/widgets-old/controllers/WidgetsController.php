<?php
// TODO Ограничить доступ гостям
namespace app\modules\widgets\controllers;

use app\components\AutoRedirect;
use Yii;
use yii\web\Controller;
use app\modules\widgets\components\WidgetsManager;

class WidgetsController extends Controller
{
    public function actionList()
    {
        $widgetsManager = new WidgetsManager;
        $widgets = $widgetsManager->getAll(Yii::$app->user->identity);

        return $this->render('list', ['widgets' => $widgets]);
    }

    public function actionAdd($base_widget_id)
    {
        $widgetsManager = new WidgetsManager;
        if ($widgetsManager->add($base_widget_id, Yii::$app->user->identity)) {
            Yii::$app->flashMessages->addSuccess('Виджет добавлен!');
        } else {
            Yii::$app->flashMessages->addError('Виджет не добавлен');
        }

        new AutoRedirect();
    }
}
