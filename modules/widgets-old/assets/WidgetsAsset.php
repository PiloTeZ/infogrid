<?php
/**
 * Created by Dmitriy Baibuhtin
 * Date: 13.02.2015
 */

namespace app\modules\widgets\assets;

use yii\web\AssetBundle;

class WidgetsAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/widgets/assets';

    public $js = [
        'js/functions.js',
        'js/scripts.js',
    ];

    public $depends = [
        'app\assets\CommonAsset',
    ];
}