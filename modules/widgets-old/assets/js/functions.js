// FUTURE Учитывание таймамутов
// TODO Задокументировать
// FUTURE Оптимизировать подгрузку ресурсов
// FUTURE Рефакторинг
// FUTURE Оптимизировать логику обработки кнопки вызова каталога и подобных
// TODO IMPORTANT Скорее всего при добавлении виджетов может оставаться мусор в виде модальных окон, скриптов и т.д.
// TODO Отменить загрузку зависимостей виджетов при каждом добавлении, если зависимость общая, вынести в CommonAsset

/*
 Решение проблемы скриптов и стилей в таблице виджетов
 1. Вариант: собирать все ресурсы виджета в помеченный блок и при удалении виджета и просто удалять этот блок, но остануться зависимости,
 например jquery скорее всего оставит в памяти id модальных окон, поэтому см. пункт 2
 2. Вариант: помечать все теги созданные виджетом и деинициализировать виджеты и удалять теги при удалении виджета ()
 В любом случае нужно вырезать ресурсы из html виджета и вставлять их в начале и конец документа, не засоряя таблицу, это самое важное
 (создаём виртуальный объект, получаем html тегов до виджета, html виджета, html тегов после виджета)
 */

/**
 * Для работы требуется:
 * тег содержащий виджеты
 * каждый виджет должен иметь класс widget и аттрибут data-widget-id
 */

/**
 * Управление коллекцией виджетов
 * @param widgetsContainer string Селектор контейнера виджетов
 * @constructor
 */
function WidgetsCollection(widgetsContainer) {
    this.widgetsContainer = $(widgetsContainer);
    this.updateCollection();
}

WidgetsCollection.prototype = {
    widgetsContainer: null,
    widgets: {},

    updateCollection: function () {
        var context = this;
        if (this.widgetsContainer.length == 0) {
            throw 'Контейнер виджетов не найден';
        }
        this.widgets = {};
        this.widgetsContainer.find('.widget').each(function (index, element) {
            var widget = $(element);
            context.widgets[widget.attr('data-widget-id')] = widget;
        });
    },
    /**
     * Добавление
     * @param widgetHtml
     * @returns integer|bool
     */
    add: function (widgetHtml) {
        this.widgetsContainer.append(widgetHtml);
        var widgetId = this._getWidgetHtmlAsObject(widgetHtml).attr('data-widget-id');
        if (widgetId) {
            this._addToList(widgetId);
            console.log('Добавлен виджет ' + widgetId);
            return widgetId;
        } else {
            throw 'Получен неверный HTML виджета';
        }
    },
    /**
     * @param widgetId integer ID виджета
     * @param widgetHtml string HTML содержимое
     */
    update: function (widgetId, widgetHtml) {
        this._find(widgetId).replaceWith(widgetHtml);
        this._updateInList(widgetId);
        console.log('Обновлен контент виджета ' + widgetId);
    },
    /**
     * @param widgetId integer ID виджета
     */
    remove: function (widgetId) {
        // По сути анимация дело WidgetsCollectionEffects, поэтому нужно вынести эту кучку отсюда
        // Как вариант передавать в класс анимаций анонимную функцию, которая должна вызваться по окончанию эффекта

        // TODO new Анимация должна выполнятся сразу, без ожидания окончания запроса к api, но есть проблема.
        // Если применять эту анимацию в классе эффектов, то виджет удалиться раньше, чем закончится анимация

        this.getWidget(widgetId).animate({
            width: 0,
            overflow: 'hidden'
        }, 300, function () {
            $(this).remove();
        });
        this.getWidget(widgetId);
        this._removeFromList(widgetId);
    },
    /**
     * Получение DOM-боъекта виджета из существующего списка
     * @param widgetId
     * @returns object|bool
     * @private
     */
    getWidget: function (widgetId) {
        var widget = this.widgets[widgetId];
        return (widget ? widget : false);
    },
    /**
     * Добавление заглушки
     * @param stubId
     */
    addStub: function (stubId) {
        this.widgetsContainer.append('<div class="widget stub" data-stub-id="' + stubId + '"><div class="loading"></div></div>');
    },
    /**
     * Удаление заглушки
     * @param stubId
     */
    removeStub: function (stubId) {
        this.widgetsContainer.find('[data-stub-id=' + stubId + ']').remove();
    },
    /**
     * Добавление виджета в список
     * @param widgetId
     * @private
     */
    _addToList: function (widgetId) {
        this.widgets[widgetId] = this._find(widgetId);
    },
    /**
     * Обновление DOM объекта виджета в списке
     * @param widgetId
     * @private
     */
    _updateInList: function (widgetId) {
        this.widgets[widgetId] = this._find(widgetId);
    },
    /**
     * Удаление виджета из списка
     * @param widgetId
     * @private
     */
    _removeFromList: function (widgetId) {
        delete this.widgets[widgetId];
    },
    /**
     * Поиск виджета в DOM
     * @param widgetId
     * @returns {*}
     * @private
     */
    _find: function (widgetId) {
        return this.widgetsContainer.find('[data-widget-id=' + widgetId + ']');
    },
    /**
     * Конвертация HTML кода в объект jQuery.
     * Для правильного парсинга HTML код должен быть обёрнут тегом
     * @param html
     * @returns {*|jQuery|HTMLElement}
     * @private
     */
    _convertHtmlToObject: function (html) {
        return $($.parseHTML(html));
    },
    /**
     * Конвертация HTML виджета в DOM объект
     * @param widgetHtml
     * @returns {*}
     * @private
     */
    _getWidgetHtmlAsObject: function (widgetHtml) {
        // TIP WidgetHtml обёрнут в div для правильного парсинга функцией $.parseHTML
        return this._convertHtmlToObject('<div>' + widgetHtml + '</div>').find('.widget');
    }
};

/**
 * API виджетов
 * @constructor
 */
function WidgetsApi() {

}

WidgetsApi.prototype = {
    getWidget: function (widgetId) {
        return $.ajax('/api/widgets/getAsHtml', {
            method: 'GET',
            data: {widget_id: widgetId},
            dataType: 'html'
        });
    },
    add: function (baseWidgetId) {
        return $.ajax('/api/widgets/add', {
            method: 'POST',
            data: {base_widget_id: baseWidgetId},
            dataType: 'html'
        });
    },
    remove: function (widgetId) {
        return $.ajax('/api/widgets/delete', {
            method: 'POST',
            data: {widget_id: widgetId},
            dataType: 'html'
        });
    },
    saveSettings: function (widgetId, settings) {
        return $.ajax('/api/widgets/save-settings?widget_id=' + widgetId, {
            method: 'POST',
            data: {widget_id: widgetId, settings: settings},
            dataType: 'html'
        });
    },
    saveOrder: function (widgetsOrder) {
        return $.ajax('/api/widgets/save-order', {
            method: 'POST',
            data: {widgets_order: widgetsOrder},
            dataType: 'html'
        });
    }
};

/**
 * Управление эффектами при манипулировании виджетами
 * @param widgetsCollection
 * @constructor
 */
function WidgetsCollectionEffects(widgetsCollection) {
    this.widgetsCollection = widgetsCollection;
}

WidgetsCollectionEffects.prototype = {
    widgetsCollection: null,
    startRemove: function (widgetId) {
        // TODO Эффект при отмене действия
        // FUTURE Упростить алгоритм
        this.widgetsCollection.getWidget(widgetId).find('.content').addClass('loading');
    },
    endRemove: function (widgetId) {

    },
    cancelRemove: function (widgetId) {
        this.widgetsCollection.getWidget(widgetId).fadeIn(300);
    },
    startAdd: function () {
        var stubId = new Date().getTime();
        this.widgetsCollection.addStub(stubId);
        return stubId;
    },
    endAdd: function (stubId) {
        this.widgetsCollection.removeStub(stubId);
    },
    cancelAdd: function (stubId) {
        this.endAdd(stubId);
    },
    startSaveSettings: function (widgetId) {
        this.widgetsCollection.getWidget(widgetId).find('.content').addClass('loading');
    },
    endSaveSettings: function (widgetId) {

    },
    cancelSaveSettings: function (widgetId) {
        this.widgetsCollection.getWidget(widgetId).find('.content').removeClass('loading');
    }
};

/**
 * Главный класс для управления виджетами
 * @constructor
 */
function WidgetsManager(params) {
    this.collection = new WidgetsCollection(params.widgetsContainer);
    this.collectionEffects = new WidgetsCollectionEffects(this.collection);
    this.api = new WidgetsApi();

    this.enableEventsApi();
}

WidgetsManager.prototype = {
    collection: false,
    collectionEffects: false,
    api: false,
    add: function (baseWidgetId) {
        console.info('Добавление виджета' + baseWidgetId);
        var context = this;
        var stubId = this.collectionEffects.startAdd();
        this.api.add(baseWidgetId)
            .done(function (data) {
                context.collectionEffects.endAdd(stubId);
                context.collection.add(data);
            })
            .fail(function () {
                alert('Виджет не добавлен');
                context.collectionEffects.cancelAdd(stubId);
            });
    },
    remove: function (widgetId) {
        console.info('Удаление виджета' + widgetId);
        var context = this;
        this.collectionEffects.startRemove(widgetId);
        this.api.remove(widgetId)
            .done(function () {
                context.collectionEffects.endRemove(widgetId);
                context.collection.remove(widgetId);
            })
            .fail(function () {
                alert('Виджет не удалён');
                context.collectionEffects.cancelRemove(widgetId);
            });
    },
    saveSettings: function (widgetId, settings) {
        console.log('Сохранение настроек виджета' + widgetId);
        var context = this;
        this.collectionEffects.startSaveSettings(widgetId);
        this.api.saveSettings(widgetId, settings)
            .done(function (data) {
                context.collection.update(widgetId, data);
                context.collectionEffects.endSaveSettings(widgetId);
            })
            .fail(function () {
                alert('Настройки не сохранены');
                context.collectionEffects.cancelSaveSettings(widgetId);
            });
    },
    /**
     * Сортировка виджетов по индексу
     * @param widgetsOrder array of ids
     */
    saveOrder: function (widgetsOrder) {
        this.api.saveOrder(widgetsOrder);
    },
    /**
     * Включение возможности вызова API виджетов через события
     */
    enableEventsApi: function () {
        var context = this;
        /**
         * Создание виджета
         * @var widgetId integer
         */
        $(document).on('add-widget', function (event, data) {
            context.add(data.widgetId);
            return false;
        });
        /**
         * Удаление виджета
         * @var widgetId integer
         */
        $(document).on('delete-widget', function (event, data) {
            context.remove(data.widgetId);
            return false;
        });
        /**
         * Сохранение настроек виджета
         * @var widgetId integer
         * @var settings object
         */
        $(document).on('save-widget-settings', function (event, data) {
            context.saveSettings(data.widgetId, data.settings);
            return false;
        });
    }
};