var widgetsContainerSelector = '#widgets';
var widgetsContainer = $(widgetsContainerSelector);
widgetsContainer.sortable({
    items: '> div',
    // Перетаскивание блока с задержкой в 5px
    distance: 5,
    // Освобождать место для блока при наведении курсором
    tolerance: 'pointer',
    // Перемещать виджет на новое место с анимацией 300мс
    revert: 300,
    // Скорость прокрутки страницы
    scrollSpeed: 5,
    // Активная область края экрана для прокрутки страницы
    scrollSensitivity: 50,
    // Класс плейсхолдера блока
    placeholder: 'widget',
    // Область виджета для перетаскивания
    handle: '.header',
    cancel: '.menu',
    update: function (event, ui) {
        console.log('Элементы отсортированы');
        widgetsManager.saveOrder(widgetsContainer.sortable('toArray', {attribute: 'data-widget-id'}));
    }
}).disableSelection();

var widgetsManager = new WidgetsManager({widgetsContainer: widgetsContainerSelector});