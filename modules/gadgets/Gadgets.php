<?php

namespace app\modules\gadgets;

class Gadgets extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\gadgets\controllers';

    public function init()
    {
        parent::init();
        $this->layout = 'inner';
        $this->layoutPath = '@app/views/layouts';
    }
}
