<?php
namespace app\modules\gadgets\controllers;

use app\modules\gadgets\components\UGadgetsManager;
use yii\web\Controller;

class UserGadgetsController extends Controller
{
    public function actionList()
    {
        $ugManager = new UGadgetsManager;

        return $this->render('gadgets', ['uGadgets' => $ugManager->find()->all()]);
    }
}