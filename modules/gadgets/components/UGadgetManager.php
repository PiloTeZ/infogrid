<?php
namespace app\modules\gadgets\components;

use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetConfig;
use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetMainConfig;
use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetGridConfig;
use app\modules\gadgets\models\entities\UGadget;
use app\widgets\base\BaseWidget;
use yii\helpers\Json;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class UGadgetManager extends GadgetManager
{
	/** @var GadgetHelper */
	public $gadgetHelper;
	/** @var UGadget */
	protected $uGadget;
	/** @var BaseGadgetMainConfig Модель конфига гаджета заполненная $this->main_config_json */
	protected $ugMainConfig;
	/** @var BaseGadgetGridConfig Модель конфига гаджета для сетки гаджетов заполненная  $this->grid_config_json */
	protected $ugGridConfig;

	/**
	 * @param integer|UGadget $uGadget
	 * @param array $config
	 * @throws ForbiddenHttpException
	 * @throws NotFoundHttpException
	 * @throws ServerErrorHttpException
	 */
	public function __construct($uGadget, $config = [])
	{
		$uGadgetsManager = new UGadgetsManager;
		$this->uGadget = $uGadgetsManager->getUGadget($uGadget);

		if (!$this->uGadget) {
			throw new NotFoundHttpException('Гаджет не найден');
		}

		parent::__construct($this->uGadget->gadget->gadget_text_id, $config);
	}

	/**
	 * Запуск пользовательского гаджета
	 * Находит гаджет и запускает его с пользовательскими параметрами
	 * @return string
	 * @throws \yii\web\ServerErrorHttpException
	 */
	public function run()
	{
		return parent::run($this->getMainConfig(), $this->getGridConfig());
	}

	/**
	 * @return string
	 */
	public function runContent()
	{
		return parent::runContent($this->getMainConfig());
	}

	/**
	 * Получение конфигурации гаджета
	 * @return BaseGadgetConfig
	 * @throws ServerErrorHttpException
	 */
	public function getMainConfig()
	{
		return $this->gadgetHelper->getPopulatedConfig(
			parent::getMainConfig(),
			Json::decode($this->uGadget->main_config_json)
		);
	}

	/**
	 * Получение конфигурации гаджетав сетке
	 * @return BaseGadgetConfig
	 * @throws ServerErrorHttpException
	 */
	public function getGridConfig()
	{
		return $this->gadgetHelper->getPopulatedConfig(
			parent::getGridConfig(),
			Json::decode($this->uGadget->grid_config_json)
		);
	}

	/**
	 * Сущность гаджета
	 * @return UGadget
	 */
	public function getUGadget()
	{
		return $this->uGadget;
	}

	/**
	 * Запуск виджета настроек гаджета
	 * @return BaseWidget
	 * @return static
	 */
	/*
		public function runSettings()
		{
			// TODO Адаптировать к новым возможностям $gadgetManager->runSettings()
			// TODO Ключевые моменты: передача form action, gridConfig, mainConfig
			$gadgetTextId = $this->gadgetHelper->getTextId();
			$gridConfig = $this->getGridConfig();
			$mainConfig = $this->getMainConfig();

			return $this->gadgetManager->runSettings($gadgetTextId);
		}
	*/
}