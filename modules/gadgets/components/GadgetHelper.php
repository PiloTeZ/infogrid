<?php

namespace app\modules\gadgets\components;

use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetConfig;
use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetMainConfig;
use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetGridConfig;
use app\modules\gadgets\gadgets\gadget\gadget\BaseGadget;
use app\modules\gadgets\gadgets\gadget\mainSettings\BaseGadgetMainSettingsWidget;
use app\modules\gadgets\gadgets\gadget\settings\GadgetSettingsWidget;
use yii\base\Object;
use yii\helpers\Inflector;
use yii\web\ServerErrorHttpException;

class GadgetHelper extends Object
{
	/** @var string Текстовый идентификатор гаджета (gadget-id) */
	protected $textId;
	/** @var string Текстовый идентификато гаджета в кэмэлкейс */
	protected $textIdCamel;
	/** @var string Текстовый идентификато гаджета в нижнем кэмэлкейс */
	protected $textIdLowerCamel;
	/** @var string Неймспейс гаджетов */
	protected $gadgetsNamespace = '\app\modules\gadgets\gadgets\\';
	/** @var string Директория гаджетов */
	protected $gadgetsDir = '@app/modules/gadgets/gadgets/';

	/**
	 * @param string $gadgetTextId
	 * @param array $config
	 * @throws ServerErrorHttpException
	 */
	public function __construct($gadgetTextId, $config = [])
	{
		$this->textId = $gadgetTextId;
		if (empty($this->textId)) {
			throw new ServerErrorHttpException('Не переданы обязательные параметры');
		}

		parent::__construct($config);
	}

	/**
	 * @return BaseGadget
	 */
	public function getGadget()
	{
		return $this->getGadgetNamespace() . 'gadget\\' . $this->getTextIdCamel() . 'Gadget';
	}

	/**
	 * @return BaseGadgetMainSettingsWidget
	 */
	public function getMainSettingsWidget()
	{
		return $this->getGadgetNamespace() . 'settings\\' . $this->getTextIdCamel() . 'GadgetSettingsWidget';
	}

	/**
	 * @return GadgetSettingsWidget
	 */
	public function getSettingsWidget()
	{
		return $this->getBaseGadgetNamespace() . 'settings\\GadgetSettingsWidget';
	}

	/**
	 * @return string
	 */
	public function getTextId()
	{
		return $this->textId;
	}

	/**
	 * @return string
	 */
	public function getTextIdCamel()
	{
		if (empty($this->textIdCamel)) {
			$this->textIdCamel = Inflector::id2camel($this->textId);
		}

		return $this->textIdCamel;
	}

	/**
	 * @return string
	 */
	public function getTextIdLowerCamel()
	{
		if (empty($this->textIdLowerCamel)) {
			$this->textIdLowerCamel = Inflector::variablize($this->textId);
		}

		return $this->textIdLowerCamel;
	}

	/**
	 * @return BaseGadgetMainConfig
	 */
	public function getMainConfigModel()
	{
		return $this->getGadgetConfigsNamespace() . $this->getTextIdCamel() . 'GadgetMainConfig';
	}

	/**
	 * @return BaseGadgetGridConfig
	 */
	public function getGridConfigModel()
	{
		return $this->getGadgetConfigsNamespace() . $this->getTextIdCamel() . 'GadgetGridConfig';
	}

	/**
	 * Получение проверенной заполненной модели конифигурации
	 * Автоматически определят как нужно получить модель конфига. Если данные пусты, вернет конфиг по-умолчанию,
	 * если массив, то создаст конфиг по-умолчанию и наполнит, если передана уже готовая модель, то просто провалидирует.
	 * Модель конфигурации проходит валидацию в любом случае
	 * @param string|BaseGadgetConfig $configModelClass
	 * @param BaseGadgetConfig|array|null $configData
	 * @return BaseGadgetConfig
	 * @throws ServerErrorHttpException
	 */
	public function getPopulatedConfig($configModelClass, $configData = null)
	{
		/** @var BaseGadgetConfig $config */
		$config = null;
		if (empty($configData)) {
			$config = new $configModelClass;
		}
		else if ($configData instanceof BaseGadgetConfig) {
			$config = $configData;
		}
		else if (is_array($configData)) {
			$config = new $configModelClass;
			$config->setAttributes($configData);
		}
		else {
			throw new ServerErrorHttpException('Неверный тип параметров гаджета');
		}

		if (!$config->validate()) {
			throw new ServerErrorHttpException('Неверные параметры гаджета');
		}

		return $config;
	}

	/**
	 * @return string
	 */
	public function getGadgetNamespace()
	{
		return $this->gadgetsNamespace . $this->getTextIdLowerCamel() . '\\';
	}

	/**
	 * @return string
	 */
	public function getGadgetConfigsNamespace()
	{
		return $this->getGadgetNamespace() . 'configs\\';
	}

	/**
	 * @return string
	 */
	public function getBaseGadgetNamespace()
	{
		return $this->gadgetsNamespace . 'gadget\\';
	}

	/**
	 * @return string
	 */
	public function getGadgetDir()
	{
		return $this->gadgetsDir . $this->getTextIdCamel() . '/';
	}

	public function getGadgetWrapperWidget()
	{
		return $this->getBaseGadgetNamespace() . 'gadgetWrapper\\GadgetWrapperWidget';
	}
}