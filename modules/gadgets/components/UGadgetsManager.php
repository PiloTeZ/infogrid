<?php
namespace app\modules\gadgets\components;

use app\components\BaseUserEntitiesManager;
use app\modules\gadgets\models\entities\UGadget;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;

class UGadgetsManager extends BaseUserEntitiesManager
{
    /**
     * Получение гаджета с проверкой прав доступа
     * Если передан ID, то гаджет получается из базы с учетом прав доступа
     * Если передан гаджет, то просто проверка прав
     * @param integer|UGadget $uGadget
     * @return array|null|\yii\db\ActiveRecord
     * @throws ForbiddenHttpException
     * @throws ServerErrorHttpException
     */
    public function getUGadget($uGadget)
    {
        if ($uGadget instanceof UGadget) {
            if ($this->checkAccess($uGadget)) {
                return $uGadget;
            } else {
                throw new ForbiddenHttpException('Доступ к гаджету запрещен');
            }
        } else if (is_integer($uGadget)) {
            return $this->find()->id($uGadget)->one();
        } else {
            throw new ServerErrorHttpException('Передан неверный аргумент');
        }
    }

    /**
     * Получение гаджетов пользователя
     * @return \app\components\ActiveQuery
     * @throws \Exception
     */
    public function find()
    {
        // TRICKY При добавлении новых фильтров для защиты нужно обязательно сделать аналог в методе checkAccess!!!
        $query = UGadget::find()->andWhere(['user_id' => $this->user->id]);

        return $query;
    }

    /**
     * Проверка наличия доступа у пользователя к гаджету
     * Для решения данной задачи вне этого класса используйте getUGadget()
     * @param UGadget $uGadget
     * @param bool|true $exception
     * @return bool
     * @throws ForbiddenHttpException
     */
    protected function checkAccess(UGadget $uGadget, $exception = true)
    {
        // TRICKY Метод должен реализовывать туже защиту, что и find(). Не больше, не меньше!
        if ($uGadget->user_id != $this->user->id) {
            if ($exception) {
                throw new ForbiddenHttpException('Доступ к гаджету запрещен');
            } else {
                return false;
            }
        }

        return true;
    }
}