<?php
namespace app\modules\gadgets\components;

use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetGridConfig;
use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetMainConfig;
use app\modules\gadgets\gadgets\gadget\gadget\BaseGadget;
use app\modules\gadgets\gadgets\gadget\gadgetWrapper\GadgetWrapperWidget;
use app\modules\gadgets\gadgets\gadget\mainSettings\GadgetSettingsWidget;
use yii\base\Object;
use yii\web\ServerErrorHttpException;

/**
 * Управление гаджетом
 * @package app\modules\gadgets\components
 */
class GadgetManager extends Object
{
	/** @var GadgetHelper */
	public $gadgetHelper;
	/** @var BaseGadgetMainConfig */
	public $defaultMainConfig;
	/** @var BaseGadgetGridConfig */
	public $defaultGridConfig;

	/**
	 * @param string $gadgetTextId Идентификатор гаджета
	 * @param array $config
	 */
	public function __construct($gadgetTextId, $config = [])
	{
		$this->gadgetHelper = new GadgetHelper($gadgetTextId);
		parent::__construct($config);
	}

	// TODO Изменить логику вывода гаджетов
	/*
	 * 1. В менеджерах сделать вывод только контента гаджетов
	 * 2. Вынести вывод кастомизированного контента гаджетов в виджеты
	 * 2.1 Список новых виджетов
	 * - gridGadget (gadgetPanel + custom grid wrapper)
	 * - gadgetPanel (вывод гаджета с меню)
	 * - ? gadgetAsBrowserExtension (гаджет для отображения в расщирении для браузера)
	 * ! Дать адекватные названия
	 */
	// TODO Не забыть о UGadgetManager
	// TODO Для получения новых гаджетов использовать helper
	// TODO Возможно сделать наследование от helper, что бы ограничить доступ для некоторых методов
	/**
	 * @param BaseGadgetMainConfig|array|null $mainConfigData
	 * @param BaseGadgetGridConfig|array|null $gridConfigData
	 * @return string
	 * @throws ServerErrorHttpException
	 * @throws \Exception
	 */
	public function run($mainConfigData = null, $gridConfigData = null)
	{
		/** @var GadgetWrapperWidget $gadget */
		$gadgetWrapperClass = $this->gadgetHelper->getGadgetWrapperWidget();
		$mainConfigClass = $this->gadgetHelper->getMainConfigModel();
		$mainConfig = $this->gadgetHelper->getPopulatedConfig($mainConfigClass, $mainConfigData);

		return $gadgetWrapperClass::widget([
			'gadgetManager' => $this,
			'gadgetContent' => $this->runContent($mainConfig),
			'gadgetTextId' => $this->gadgetHelper->getTextId(),
		]);
	}

	// TODO Переимновать в run
	/**
	 * @param BaseGadgetMainConfig|array|null $mainConfigData
	 * @return string
	 * @throws ServerErrorHttpException
	 * @throws \Exception
	 */
	public function runContent($mainConfigData = null)
	{
		$mainConfigClass = $this->gadgetHelper->getMainConfigModel();
		$mainConfig = $this->gadgetHelper->getPopulatedConfig($mainConfigClass, $mainConfigData);
		/** @var BaseGadget $gadget */
		$gadgetContentClass = $this->gadgetHelper->getGadget();

		return $gadgetContentClass::widget([
			'config' => $mainConfig,
			'textId' => $this->gadgetHelper->getTextId(),
		]);
	}

	// TODO Сделать аналогично выводу самого гаджета (?runSettings должен выводить только форму с кнопками)
	// TODO Продумать этот момент, нужен ли вообще этот метод, если нет, то не будет ли это кашей? При выводе гаджета так, при выводе настроек так
	// TODO Не забыть о UGadgetManager
	public function runSettings()
	{
		$settingsWidget = $this->gadgetHelper->getSettingsWidget();

		return $settingsWidget::widget(['gadgetManager' => $this]);
	}

	/**
	 * @return BaseGadgetMainConfig
	 * @throws ServerErrorHttpException
	 */
	public function getMainConfig()
	{
		if (empty($this->defaultMainConfig)) {
			$configModelClass = $this->gadgetHelper->getMainConfigModel();
			$this->defaultMainConfig = $this->gadgetHelper->getPopulatedConfig($configModelClass);
		}

		// clone что бы случайно не изменить модель в кэше (объекты всегда передаются по ссылке)
		return clone $this->defaultMainConfig;
	}

	/**
	 * @return BaseGadgetGridConfig
	 * @throws ServerErrorHttpException
	 */
	public function getGridConfig()
	{
		if (empty($this->defaultGridConfig)) {
			$configModelClass = $this->gadgetHelper->getGridConfigModel();
			$this->defaultGridConfig = $this->gadgetHelper->getPopulatedConfig($configModelClass);
		}

		// clone что бы случайно не изменить модель в кэше (объекты всегда передаются по ссылке)
		return clone $this->defaultGridConfig;
	}

	/**
	 * @param array $customParams
	 * @return GadgetSettingsWidget
	 */
	/*
	public function runSettings($customParams = [])
	{
		// TODO Сделать заново
		// TODO Адаптировать метод к новым возможностям виджета настроек
		$defaultParams = [
			'gridSettingsWidget' => true,
			'mainSettingsWidget' => true,
			'previewWidget' => true,
		];
		$params = array_merge($defaultParams, $customParams);

		$settingsWidgetClass = $this->helper->getSettingsWidget();
		$settingsWidget = $settingsWidgetClass::begin([
			'gadgetTextId' => $this->helper->getTextId(),
			'gridSettingsWidget' => $params['gridSettingsWidget'],
			'mainSettingsWidget' => $params['mainSettingsWidget'],
			'previewWidget' => $params['previewWidget'],
		]);
		$settingsWidget->end();

		return $settingsWidget;
	}
	*/
}