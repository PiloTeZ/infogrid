<?php

namespace app\modules\gadgets\models\entities;

use Yii;

/**
 * Гаджет
 *
 * @property integer $id
 * @property string $gadget_text_id
 * @property string $name
 * @property string $description
 *
 * @property UGadget $usersGadgets
 */
class Gadget extends \app\models\Entity
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gadgets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gadget_text_id', 'name'], 'required'],
            [['gadget_text_id'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 128],
            [['description'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gadget_text_id' => 'Gadget ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    public function getUsersGadgets()
    {
        return $this->hasMany(UGadget::className(), ['gadget_id' => 'id']);
    }
}
