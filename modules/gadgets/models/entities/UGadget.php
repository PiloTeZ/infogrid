<?php

namespace app\modules\gadgets\models\entities;

use app\modules\gadgets\models\queryBuilders\UsersGadgetsQb;
use Yii;
use yii\helpers\Json;

/**
 * Пользовательский гаджет
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $gadget_id
 * @property string $main_config_json
 * @property string $grid_config_json
 *
 * @property Gadget $gadget
 */
class UGadget extends \app\models\Entity
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_gadgets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'gadget_id'], 'required'],
            [['id', 'user_id', 'gadget_id'], 'integer'],
            [['main_config_json', 'grid_config_json'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'gadget_id' => 'Gadget Catalog ID',
            'main_config_json' => 'Config Json',
            'grid_config_json' => 'Grid Config Json',
        ];
    }

    /**
     * Связь с гаджетами
     * @return \app\components\ActiveQuery
     */
    public function getGadget()
    {
        return $this->hasOne(Gadget::className(), ['id' => 'gadget_id']);
    }
}
