<?php

namespace app\modules\gadgets\gadgets\example\configs;

use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetMainConfig;

class ExampleGadgetMainConfig extends BaseGadgetMainConfig
{
    public $title = 'Пример';
}