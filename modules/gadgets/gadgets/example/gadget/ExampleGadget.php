<?php

namespace app\modules\gadgets\gadgets\example\gadget;

use app\modules\gadgets\gadgets\gadget\gadget\BaseGadget;

class ExampleGadget extends BaseGadget
{
    protected $widgetUseAngularJsController = true;
}