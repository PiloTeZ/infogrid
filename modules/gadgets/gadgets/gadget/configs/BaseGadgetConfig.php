<?php
/**
 * Created by PhpStorm.
 * User: PiloT
 * Date: 03.08.2015
 * Time: 23:13
 */

namespace app\modules\gadgets\gadgets\gadget\configs;

use yii\base\Model;

/**
 * Базовый класс для всех конфигов
 */
abstract class BaseGadgetConfig extends Model
{

}