<?php

namespace app\modules\gadgets\gadgets\gadget\configs;

class BaseGadgetMainConfig extends BaseGadgetConfig
{
    public $title;

    public function rules()
    {
        return [
            [['title'], 'required'],
        ];
    }
}