<?php
namespace app\modules\gadgets\gadgets\gadget\configs;

abstract class BaseGadgetGridConfig extends BaseGadgetConfig
{
    public $positionX = 1;
    public $positionY = 1;
}