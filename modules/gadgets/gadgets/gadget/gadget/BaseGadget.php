<?php

namespace app\modules\gadgets\gadgets\gadget\gadget;

use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetMainConfig;
use app\widgets\base\BaseWidget;
use yii\helpers\Html;

abstract class BaseGadget extends BaseWidget
{
	/** @var BaseGadgetMainConfig */
	public $config;
	/** @var string Текстовый идентификатор гаджета */
	public $textId;

	protected function beforeRender(&$view, &$params, &$widgetContent)
	{
		$gadgetDivParams = [];
		$gadgetDivParams['id'] = $this->id;
		$gadgetDivParams['class'] = 'gadget-' . $this->textId . '-content';
		$widgetContent .= Html::beginTag('div', $gadgetDivParams);
		parent::beforeRender($view, $params, $widgetContent);
	}

	protected function afterRender(&$view, &$params, &$widgetContent)
	{
		parent::afterRender($view, $params, $widgetContent);
		$widgetContent .= Html::endTag('div');
	}

	public function run($view = 'gadget', $params = [])
	{
		return $this->render($view, $params);
	}
}