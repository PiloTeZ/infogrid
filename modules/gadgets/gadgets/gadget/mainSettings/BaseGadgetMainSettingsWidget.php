<?php
namespace app\modules\gadgets\gadgets\gadget\mainSettings;

use app\modules\gadgets\gadgets\gadget\baseSettings\BaseGadgetSettingsWidget;

class BaseGadgetMainSettingsWidget extends BaseGadgetSettingsWidget
{
	public function run()
	{
		$this->configClass = $this->gadgetManager->gadgetHelper->getMainConfigModel();

		return parent::run();
	}
}