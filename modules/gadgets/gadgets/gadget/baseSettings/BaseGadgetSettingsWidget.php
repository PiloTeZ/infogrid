<?php
namespace app\modules\gadgets\gadgets\gadget\baseSettings;

use app\modules\gadgets\components\GadgetManager;
use app\modules\gadgets\gadgets\gadget\configs\BaseGadgetConfig;
use app\widgets\base\BaseWidget;
use yii\widgets\ActiveForm;

abstract class BaseGadgetSettingsWidget extends BaseWidget
{
	/** @var GadgetManager Гаджет */
	public $gadgetManager;
	/** @var BaseGadgetConfig */
	protected $configClass;
	/** @var string DOM Идентификатор формы настроек */
	protected $formDomId;
	/** @var bool Наличие в форме полей загрузки файлов
	 * Используется для установки правильного enctype формы */
	protected $formHasFileInputs = false;

	public function init()
	{
		if (empty($this->gadgetManager) || !$this->gadgetManager instanceof GadgetManager) {
			throw new \InvalidArgumentException('BaseGadgetSettingsWidget::gadgetManager');
		}
		parent::init();
	}

	public function run()
	{
		if (empty($this->configClass)) {
			throw new \InvalidArgumentException('BaseGadgetSettingsWidget::configClass');
		}

		$this->formDomId = 'gadget-main-settings-form-' . $this->id;

		$formParams = ['options' => ['id' => $this->formDomId]];
		// Установка правильного enctype для файловых форм
		if ($this->formHasFileInputs) {
			$formParams['options']['enctype'] = 'multipart/form-data';
		}

		$params = [
			// Для удобства предустанавливается в базовом виджете
			'form' => ActiveForm::begin($formParams),
			'config' => new $this->configClass,
		];

		return $this->render('settings', $params);
	}

	public function getFormDomId()
	{
		return $this->formDomId;
	}

	protected function afterRender(&$view, &$params, &$widgetContent)
	{
		ob_start();
		$params['form']->end();
		$widgetContent .= ob_get_clean();

		parent::afterRender($view, $params, $widgetContent);
	}
}