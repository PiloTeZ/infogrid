<?
/** @var \app\modules\gadgets\gadgets\gadget\configs\BaseGadgetMainConfig $gadgetMainConfig */
/** @var \app\modules\gadgets\gadgets\gadget\configs\BaseGadgetGridConfig $gadgetGridConfig */
/** @var string $gadgetContent */
/** @var string $gadgetDomId Идентификатор гаджета в DOM */
/** @var string $gadgetTextId Текстовый идентификатор гаджета */
?>

<? $settingsButtonId = 'gadget-settings-button-' . $widgetId ?>

<? $this->registerJs(<<<JS
	jQuery('#{$settingsButtonId}').on('click', function() {
	  	jQuery('#{$gadgetSettingsDialog->id}').dialog( "open" );
	});
JS
) ?>

<div id="<?= $widgetId ?>" class="gadget gadget-<?= $gadgetTextId ?>">
	<div class="gadget__header gadget-header gadget-<?= $gadgetTextId ?>-header">
		<div class="gadget-header__icon"><img src="" alt=""></div>
		<div class="gadget-header__title gadget-header-title">
			<span class="gadget-header-title__text"><?= $gadgetMainConfig->title ?></span>
		</div>
		<div class="gadget-header__menu gadget-header-menu">
			<div class="gadget-header-menu__item gadget-header-menu-item">
				<a href="#" class="gadget-header-menu-item__link">E</a>
			</div>
			<div class="gadget-header-menu__item gadget-header-menu-item">
				<a href="#" class="gadget-header-menu-item__link" id="<?= $settingsButtonId ?>">S</a>
			</div>
		</div>
	</div>
	<div class="gadget__content gadget-content gadget-<?= $gadgetTextId ?>-content">
		<?= $gadgetContent ?>
	</div>
</div>