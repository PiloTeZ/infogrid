<?php
/**
 * Created by PhpStorm.
 * User: PiloT
 * Date: 02.08.2015
 * Time: 15:54
 */

namespace app\modules\gadgets\gadgets\gadget\gadgetWrapper;

use app\modules\gadgets\components\GadgetManager;
use app\widgets\base\BaseWidget;

class GadgetWrapperWidget extends BaseWidget
{
	/** @var GadgetManager */
	public $gadgetManager;
	/** @var string HTML код гаджета */
	public $gadgetContent;
	/** @var string Текстовый идентификатор гаджета */
	public $gadgetTextId;

	public function run()
	{
		$gadgetSettingsDialog = \yii\jui\Dialog::begin([
			'clientOptions' => [
				'modal' => true,
				'autoOpen' => false,
				'title' => 'Настройки',
			]
		]);
		echo $this->gadgetManager->runSettings();
		$gadgetSettingsDialog->end();

		return $this->render('wrapper', [
			'gadgetSettingsDialog' => $gadgetSettingsDialog,
			'gadgetMainConfig' => $this->gadgetManager->getMainConfig(),
			'gadgetGridConfig' => $this->gadgetManager->getGridConfig(),
			'gadgetContent' => $this->gadgetContent,
			'gadgetTextId' => $this->gadgetTextId,
		]);
	}
}