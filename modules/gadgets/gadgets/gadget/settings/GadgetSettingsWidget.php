<?php

namespace app\modules\gadgets\gadgets\gadget\settings;

use app\modules\gadgets\components\GadgetManager;
use app\widgets\base\BaseWidget;

class GadgetSettingsWidget extends BaseWidget
{
	/** @var GadgetManager */
	public $gadgetManager;

	public function run()
	{
		$gadgetMainSettingsWidgetClass = $this->gadgetManager->gadgetHelper->getMainSettingsWidget();

		return $this->render('settings', [
			'gadgetMainSettingsWidgetClass' => $gadgetMainSettingsWidgetClass,
			'gadgetManager' => $this->gadgetManager,
		]);
	}
}