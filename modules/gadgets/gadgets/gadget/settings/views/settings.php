<?
/** @var \app\modules\gadgets\gadgets\gadget\mainSettings\BaseGadgetMainSettingsWidget $gadgetMainSettingsWidgetClass */
/** @var \app\modules\gadgets\gadgets\gadget\mainSettings\BaseGadgetMainSettingsWidget $mainSettingsWidget */
/** @var \app\modules\gadgets\components\GadgetManager $gadgetManager */
/** @var string $widgetId */
?>
	<div id="gadget-settings-<?= $widgetId ?>" class="gadget-settings">
		<div id="gadget-settings-<?= $widgetId ?>-forms" class="gadget-settings__forms">
			<? $mainSettingsWidget = $gadgetMainSettingsWidgetClass::begin(['gadgetManager' => $gadgetManager]) ?>
			<? $mainSettingsWidget->end() ?>
			<? $mainSettingsFormId = $mainSettingsWidget->getFormDomId() ?>
			<div class="gadget-settings-buttons">
				<div class="gadget-settings-buttons__button-wrapper">
					<input class="gadget-settings-buttons__button gadget-settings-buttons__button_save" type="submit"
						   value="Сохранить"/>
				</div>
				<div class="gadget-settings-button__wrapper">
					<input class="gadget-settings-buttons__button gadget-settings-buttons__button_preview" type="button"
						   value="Предпросмотр"/>
				</div>
			</div>
		</div>
		<div class="gadget-settings__preview">
			<?= $gadgetManager->runContent() ?>
		</div>
	</div>

<? $this->registerJs(<<<JS
	var jqComplexForm = $('#gadget-settings-{$widgetId}-forms');
	var complexForm{$widgetId} = new ComplexForm(jqComplexForm, {submit: '.gadget-settings-buttons__button_save'});

	jqComplexForm.find('.gadget-settings-buttons__button_preview').on('click', function () {
		// TODO Когда будет реализован JS класс и API для управления гаджетами в DOM (добавить, удалить, обновить),
		// тогда переписать логику получения превью гаджета
		console.info('Обновление превью');
		/*
		if (complexForm.validate()) {
		complexForm.getData();
		}
		*/
	});
JS
);
