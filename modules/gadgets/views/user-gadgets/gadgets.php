<?
/** @var array $uGadgets [UGadget] */
// /** @var array $uGadgetsGrid[UGadget->id][] Массив с дополнительными параметрами гаджетов. Пока не требуется */
?>

<div class="gadgets-grid">
	<?
	$gadgetsCounter = 0;
	$columnsCounter = 0;
	/** @var \app\modules\gadgets\models\entities\UGadget $uGadget */
	foreach ($uGadgets as $uGadget) { ?>
		<? $gadgetsCounter++ ?>
		<? $isNewColumnStart = $gadgetsCounter == 1 || $gadgetsCounter % 5 == 0 ?>
		<? $isNewColumnEnd = ($isNewColumnStart && $gadgetsCounter != 1) || $gadgetsCounter % 5 == 0 || $gadgetsCounter == count($uGadgets) ?>
		<? if ($isNewColumnStart) { ?>
			<? $columnsCounter++ ?>
			<div class="gadgets-grid__column gadgets-grid__column_<?= $columnsCounter ?>">
		<? } ?>
		<div class="gadgets-grid__item">
			<? $uGadgetManager = new \app\modules\gadgets\components\UGadgetManager($uGadget) ?>
			<?= $uGadgetManager->run() ?>
		</div>
		<? if ($isNewColumnEnd) { ?>
			</div>
		<? } ?>
	<? } ?>
</div>
