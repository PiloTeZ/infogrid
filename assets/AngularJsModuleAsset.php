<?php
/**
 * Created by Dmitriy Baibuhtin
 * Date: 13.02.2015
 */

namespace app\assets;

use yii\web\AssetBundle;

class AngularJsModuleAsset extends AssetBundle
{
    public $sourcePath = '@app/assets';

    public $js = [
        'common/js/angular-js-module.js',
    ];

    public $depends = [
        'app\assets\AngularJsAsset',
    ];
}