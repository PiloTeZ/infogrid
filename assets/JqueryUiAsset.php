<?php


namespace app\assets;

use yii\web\AssetBundle;

class JqueryUiAsset extends AssetBundle
{
    public $css = [
        'plugins/jquery-ui/jquery-ui.min.css',
    ];

    public $js = [
        'plugins/jquery-ui/jquery-ui.min.js',
        'plugins/jquery-ui/datepicker-ru.js',
    ];

    public $depends = [
        '\yii\web\JqueryAsset',
    ];
}