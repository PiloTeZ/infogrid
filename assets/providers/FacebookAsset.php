<?php
/**
 * Created by Dmitriy Baibuhtin
 * Date: 13.02.2015
 */

namespace app\assets\ProvidersAssets;

use yii\web\AssetBundle;

class FacebookAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/js/api-providers/providers';

    public $js = [
        'facebook-provider.js'
    ];

    public $depends = [
        'app\assets\ApiProvidersAsset',
    ];
}