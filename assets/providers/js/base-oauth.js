function BaseOAuth(tokenData) {
    BaseProvider.apply(this, [tokenData]);
    this.accessToken = new OAuthToken(tokenData);
    this.version = null;
    this.apiBaseUrl = null;
    /* this._signatureMethod = null; */
}

BaseOAuth.prototype = Object.create(BaseProvider.prototype);
BaseOAuth.prototype.api = function (apiSubUrl, params, method, headers) {
    var url;
    params = (angular.isObject(params) ? params : {});
    method = (angular.isUndefined(method) ? 'GET' : method);
    headers = (angular.isObject(headers) ? headers : {});

    if (apiSubUrl.match('/^https?:\\/\\//is')) {
        url = apiSubUrl;
    } else {
        url = this.apiBaseUrl + '/' + apiSubUrl;
    }

    return this._apiInternal(url, params, method, headers);
};

BaseOAuth.prototype._apiInternal = function (url, params, method, headers) {
    throw 'Метод не реализован';
};

BaseOAuth.prototype._sendRequest = function (url, params, method, headers) {
    return $.ajax(url, {
        data: params,
        type: method,
        headers: headers,
        dataType: 'jsonp'
    });
};

/*
 BaseOAuth.prototype.getSignatureMethod = function()
 {
 if (!angular.isObject(this._signatureMethod)) {
 this._signatureMethod = this.createSignatureMethod(this._signatureMethod);
 }

 return this._signatureMethod;
 };

 BaseOAuth.prototype.createSignatureMethod = function(signatureMethodConfig)
 {
 // :TODO
 };
 */