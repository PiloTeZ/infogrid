function YandexProvider(providerData) {
    OAuth2.apply(this, [providerData]);
    this.id = 'google';
    this.name = 'Google';
    this.apiBaseUrl = 'https://login.yandex.ru';
}
YandexProvider.prototype = Object.create(OAuth2.prototype);