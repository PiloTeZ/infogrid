function VkontakteProvider(providerData) {
    OAuth2.apply(this, [providerData]);
    this.id = 'vkontakte';
    this.name = 'Вконтакте';
    this.apiBaseUrl = 'https://api.vk.com/method';
}
VkontakteProvider.prototype = Object.create(OAuth2.prototype);