function TwitterProvider(accountId) {
    BaseProxyProvider.apply(this, [accountId]);
    this.id = 'twitter';
    this.name = 'Twitter';
}
TwitterProvider.prototype = Object.create(BaseProxyProvider.prototype);