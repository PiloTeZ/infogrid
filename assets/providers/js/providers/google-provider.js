function GoogleProvider(providerData) {
    OAuth2.apply(this, [providerData]);
    this.id = 'google';
    this.name = 'Google';
    this.apiBaseUrl = 'https://www.googleapis.com/plus/v1';
}
GoogleProvider.prototype = Object.create(OAuth2.prototype);