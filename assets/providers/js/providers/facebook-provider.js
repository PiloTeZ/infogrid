function FacebookProvider(providerData) {
    OAuth2.apply(this, [providerData]);
    this.id = 'facebook';
    this.name = 'Facebook';
    this.apiBaseUrl = 'https://graph.facebook.com';
}
FacebookProvider.prototype = Object.create(OAuth2.prototype);