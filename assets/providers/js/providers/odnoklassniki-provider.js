function OdnoklassnikiProvider(accountId) {
    BaseProxyProvider.apply(this, [accountId]);
    this.id = 'odnoklassniki';
    this.name = 'Одноклассники';
}
OdnoklassnikiProvider.prototype = Object.create(BaseProxyProvider.prototype);