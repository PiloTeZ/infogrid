function OAuth2(providerData) {
    BaseOAuth.apply(this, [providerData]);
    this.version = '2.0';
}

OAuth2.prototype = Object.create(BaseOAuth.prototype);
OAuth2.prototype._apiInternal = function (url, params, method, headers) {
    params.access_token = this.accessToken.token;
    return this._sendRequest(url, params, method, headers);
};