function BaseProxyProvider(accountId) {
    this._proxyUrl = '/web/index.php?r=api-providers-proxy/api';
    this._providerAccountId = accountId;
}

BaseProxyProvider.prototype = Object.create(BaseProvider.prototype);

/**
 * @param apiSubUrl
 * @param params
 * @param method
 * @param headers
 * @returns {*}
 */
BaseProxyProvider.prototype.api = function (apiSubUrl, params, method, headers) {
    var data = {
        providerAccountId: this._providerAccountId,
        apiSubUrl: apiSubUrl,
        params: params,
        method: method,
        headers: headers
    };

    data[yii.getCsrfParam()] = yii.getCsrfToken();

    return $.ajax(this._proxyUrl, {
        data: data,
        type: 'POST',
        dataType: 'json'
    });
};
