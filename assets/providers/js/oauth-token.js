function OAuthToken(tokenData) {
    this.token = tokenData.token;
    this.expireDuration = tokenData.expireDuration;
    this.createTimestamp = tokenData.createTimestamp;
    /* OAuth 1 this.tokenSecret = (tokenData.tokenSecret ? tokenData.tokenSecret : null); */
}

OAuthToken.prototype.isValid = function() {
    if (this.token != '' && !this.isExpired()) {
        return true;
    }
};

OAuthToken.prototype.isExpired = function () {
    if (this.expireDuration == '') {
        return false;
    }

    return (time() >= this.createTimestamp + this.expireDuration);
};