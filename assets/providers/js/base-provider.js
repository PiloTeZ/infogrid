function BaseProvider() {
    /** @var Текстовый системный идентификатор провайдера **/
    this.id = null;
    /** @var Название провайдера для отображения **/
    this.name = null;
}