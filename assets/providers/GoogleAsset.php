<?php
/**
 * Created by Dmitriy Baibuhtin
 * Date: 13.02.2015
 */

namespace app\assets\ProvidersAssets;

use yii\web\AssetBundle;

class GoogleAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/js/api-providers/providers';

    public $js = [
        'google-provider.js'
    ];

    public $depends = [
        'app\assets\ApiProvidersAsset',
    ];
}