<?php
/**
 * Created by Dmitriy Baibuhtin
 * Date: 13.02.2015
 */

namespace app\assets;

use yii\web\AssetBundle;

class AuthProvidersAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/providers/js/providers';

    public $js = [
        'base-provider.js',
        'base-proxy-provider.js',
        'oauth-token.js',
        'base-oauth.js',
        /* 'oauth-1.js', */
        'oauth-2.js',
    ];

    public $depends = [
        'app\assets\CommonAsset',
    ];
}