$(function () {
    $.widget("custom.table", {
        options: {
            size: {
                width: null,
                height: null
            },
            elementsSelector: '> div',
            elementIdAttr: 'table-id',
            elementWidthAttr: 'table-width',
            elementHeightAttr: 'table-height'
        },
        table: {},
        elements: {},
        _create: function () {
            this.table = new Table({
                size: this.options.size
            });
            this.elements = new Elements({
                container: this.element,
                elementsSelector: this.options.elementsSelector,
                elementIdAttr: this.options.elementIdAttr,
                elementWidthAttr: this.options.elementWidthAttr,
                elementHeightAttr: this.options.elementHeightAttr
            });

            this.arrangeItems(this.elements.getElements());
        },
        arrangeItems: function (elements) {
            var self = this;
            $.each(elements, function (i, element) {
                self.arrangeItem(element);
            });
        },
        arrangeItem: function (element) {
            var coords = this.table.findFreeArea(element.size);
            this.elements.setElementCoords(element.id, coords);
        }
    });

    function Table(options) {
        this.options = $.extend(this.defaultOptions, options);
        // Разобраться на кой хер это тут, скорее всего при изменении структуры таблицы надо будет пресчитывать этот бред
        this.width = this.options.size.width;
        this.height = this.options.size.height;
        this.generateTable();
    }

    Table.prototype = {
        TOP: 'top',
        BOTTOM: 'bottom',
        LEFT: 'left',
        RIGHT: 'right',
        options: {},
        defaultOptions: {
            width: null,
            height: null
        },
        width: null,
        height: null,
        table: [],
        findFreeArea: function (size) {
            var lastObjectCoords = this.findLastObject();
        },
        findLastObject: function () {

        },
        getCell: function (rowIndex, cellIndex) {

        },
        generateTable: function () {
            for (var rowI = 0; rowI < this.height; rowI++) {
                this.addRow();
            }
            return true;
        },
        addRow: function (side) {
            var rowIndex;
            side = getVal(side, this.BOTTOM);
            if (side == this.BOTTOM) {
                this.table.push([]);
                rowIndex = this.getTableSize().height - 1;
            } else if (side == this.TOP) {
                this.table.unshift([]);
                rowIndex = 0;
            } else {
                throw 'Указана неверная позиция для добавления строки';
            }

            for (var cells = 0; cells < this.width; cells++) {
                this._addCell(rowIndex);
            }
            return rowIndex;
        },
        getTableSize: function () {
            return {width: this.getRowSize(0), height: this.table.length};
        },
        getRowSize: function (rowIndex) {
            return this.table[rowIndex].length;
        },
        _addCell: function (rowIndex, side) {
            var cellIndex;
            side = getVal(side, this.RIGHT);
            if (side == this.RIGHT) {
                this.table[rowIndex].push([]);
                cellIndex = this.getRowSize(rowIndex) - 1;
            } else if (side == this.LEFT) {
                this.table[rowIndex].unshift([]);
                cellIndex = 0;
            } else {
                throw 'Указана неверная позиция для добавления ячейки';
            }

            return cellIndex;
        }
    };

    function Elements(options) {
        this.options = $.extend(this.defaultOptions, options);
        this._setContainer(options.container);
        this._addBySelector(this.options.elementsSelector);
    }

    Elements.prototype = {
        options: {},
        defaultOptions: {
            container: null,
            elementsSelector: '> div',
            elementIdAttr: 'table-id',
            elementWidthAttr: 'table-width',
            elementHeightAttr: 'table-height'
        },
        $container: {},
        elements: {},
        getElements: function () {
            return this.elements;
        },
        _addBySelector: function (selector) {
            var self = this;
            this.$container.find(selector).each(function (i, element) {
                var $element = $(element);
                var elementId = $element.data(self.options.elementIdAttr);
                self.elements[elementId] = {
                    $element: $element,
                    id: elementId,
                    size: {
                        width: $element.data(self.options.elementWidthAttr),
                        height: $element.data(self.options.elementHeightAttr)
                    }
                };
            });
            return true;
        },
        _setContainer: function (container) {
            this.$container = (typeof container == 'object' ? container : $(container));
        }
    };

    function getVal(value, defaultValue) {
        return (typeof value == 'undefined' ? defaultValue : value);
    }
});

/**
 * Управление таблицей
 * добавление строк
 * добавление колонок
 * ----
 * удаление строк
 * удаление колонок
 * =====
 * добавление/удаление ячеек
 */

/**
 * Управление элементами
 *
 * добавление
 * удаление
 */