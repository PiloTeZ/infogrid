// Сделать API и уже на API делать плагин. Как можно меньше копипаста, задокументировать


/**
 * Старт:
 * Создание списка элементов (запись $ и переданных позиций + проверка занятости, иначе удаление позиций)
 * Генерация позиций для оставшихся элементов
 *
 */
// TODO Заменить width, height на size
/**
 * Позиция (position) - абсолютное положение элемента в px относительно сетки
 * Координаты (coords) - координаты ячейки в таблице; y - строка, x - ячейка
 */
$(function () {
    $.widget("custom.grid", {
        // default options
        options: {
            width: null,
            height: null,
            itemsSelector: '> *',
            itemSize: 100,
            itemMargin: 10
            // callbacks
        },

        /**
         *
         */
        _items: [],
        /**
         * строка: {ячейка: элемент, ячейка: элемент, ячейка: элемент}
         * строка: {ячейка: элемент, ячейка: элемент, ячейка: элемент}
         * строка: {ячейка: элемент, ячейка: элемент, ячейка: элемент}
         */
        _grid: [],
        /**
         *
         * @private
         */
        _create: function () {
            var self = this;
            this.setGrid(this.element);

            // Генерация сетки
            this.generateGrid(this.options.width, this.options.height);
            // Чтение элементов
            this.refreshItems();
            // Генерация позиций
            this.arrangeItems(this._items);
            $('.logo a').on('click', function () {
                self.printGrid();
                console.log(self);
                return false;
            });
        },
        /**
         *
         */
        refreshItems: function () {
            var self = this;
            this._items = [];
            this.getElements().each(function (i, element) {
                var $element = $(element);
                var width = Number($element.attr('data-grid-width'));
                var height = Number($element.attr('data-grid-height'));
                $element.css('width', self.calcItemSize(width));
                $element.height(self.calcItemSize(height));
                self._items.push({
                    element: $element,
                    width: width,
                    height: height
                });
            });
        },
        calcItemSize: function (size) {
            return (size * this.options.itemSize + this.options.itemMargin * (size - 1));
        },
        /**
         *
         * @param items
         */
        arrangeItems: function (items) {
            var self = this;
            $.each(items, function (key, item) {
                self.arrangeItem(key, item);
            });
        },
        /**
         *
         * @param key
         * @param item
         */
        arrangeItem: function (key, item) {
            var coords = this.findPlace(item);
            if (!coords) {
                console.log('Элемент ' + item.width + '/' + item.height + ' не вместился в текущую таблицу');
            } else {
                this.setItemCoords(key, coords);
            }
        },
        findPlace: function (item) {
            var freeCell = null;
            var test = 10;
            while (test) {
                freeCell = this.findFreeCell((freeCell ? {x: (freeCell.x), y: (freeCell.y)} : null));
                if (!freeCell) {
                    return false;
                }
                if (this.checkCoords(freeCell, item)) {
                    // TODO Убрать лишнюю функцию findFreeCell
                    return freeCell;
                }
                test--;
            }
        },
        // TODO Заменить на itemKey и getItem
        checkCoords: function (coords, item) {
            var cell;
            var maxRow = coords.y + item.height;
            var maxCell = coords.x + item.width;

            if (maxRow > this._grid.length) {
                maxRow = this._grid.length;
            }

            // Будут проблемы при удалении первой строки сетки
            if (maxCell > this._grid[1].length) {
                maxCell = this._grid[1].length;
            }

            for (var row = coords.y; row < maxRow; row++) {
                for (cell = coords.x; cell < maxCell; cell++) {
                    if (this._grid[row][cell] !== null) {
                        return false;
                    }
                }
            }

            return true;
        },
        findFreeCell: function (coordsStart) {
            var cell, startRow, startCell;
            if (coordsStart) {
                startRow = coordsStart.y;
                startCell = coordsStart.x;
            } else {
                startRow = 0;
                startCell = 0;
            }

            for (var row = startRow; row < this._grid.length; row++) {
                for (cell = startCell; cell < this._grid[row].length; cell++) {
                    if (this._grid[row][cell] === null) {
                        return {x: cell, y: row};
                    }
                }
            }
            return false;
        },
        // TODO Заменить везде key, item на просто itemKey
        setItemCoords: function (itemKey, coords) {
            var cell;
            var item = this._items[itemKey];
            var $item = item.element;
            var itemPosition = this.calcItemPosition(coords);
            $item.css({
                left: itemPosition.x,
                top: itemPosition.y
            });
            item.coords = coords;
            this._grid[coords.y][coords.x] = itemKey;

            for (var row = coords.y; row < (coords.y + item.height); row++) {
                for (cell = coords.x; cell < (coords.x + item.width); cell++) {
                    this._grid[row][cell] = itemKey;
                }
            }
        },
        // TODO Заменить coords на itemKey
        /**
         * Получение позиции виджета
         * @param coords
         * @returns {{x: number, y: number}}
         */
        calcItemPosition: function (coords) {
            return {
                x: ((coords.x - 1) * this.options.itemSize + this.options.itemMargin * (coords.x - 1)),
                y: ((coords.y - 1) * this.options.itemSize + this.options.itemMargin * (coords.y - 1))
            };
        },
        /**
         *
         * @param width
         * @param height
         */
        generateGrid: function (width, height) {
            this._grid = [];
            for (var row = 0; row < height; row++) {
                this._grid[row] = [];
                for (var cell = 0; cell < width; cell++) {
                    this._grid[row][cell] = null;
                }
            }
        },
        /**
         *
         * @returns {*}
         */
        getElements: function () {
            return this.element.find(this.options.itemsSelector);
        },

        /**
         * Получение ячеек
         * @param beginRow number
         * @param endRow number
         * @param beginCell number
         * @param endCell number
         * @returns {Array}
         */
        getCells: function (beginRow, endRow, beginCell, endCell) {
            var cells = [];
            var gridSize = this.getGridSize();
            beginRow = (typeof beginRow == 'undefined' || beginRow > gridSize.height ? gridSize.height : beginRow);
            beginCell = (typeof endCell == 'undefined' || beginCell > gridSize.width ? gridSize.width : beginCell);
            endRow = (typeof endRow == 'undefined' || endRow > gridSize.height ? gridSize.height : endRow);
            endCell = (typeof endCell == 'undefined' || endCell > gridSize.width ? gridSize.width : endCell);

            for (var rowId = beginRow; rowId < endRow; rowId++) {
                for (var cellId = beginCell; cellId < endCell; cellId++) {
                    cells.push({row: rowId, cell: cellId});
                }
            }

            return cells;
        },
        /**
         * Получение области ячеек
         * @param x number Начальная ячейка
         * @param y number Начальная строка
         * @param width number Ширина области
         * @param height number Высота области
         * @param strict bool true Обязательное наличие всех ячеек
         * return {Array}|bool
         */
        getCellsArea: function (x, y, width, height, strict) {

            this.getCells(y, y + height, x, x + height);
        },
        getCell: function (cell, row) {
            return (typeof this._grid[row][cell] == 'undefined' ? false : this._grid[row][cell]);
        },
        getGridSize: function () {
            return {width: this._grid[0].length, height: this._grid.length};
        },
        setGrid: function (grid) {
            this._$grid = (typeof grid == 'object' ? grid : $(grid));
        },
        hideGrid: function () {
            this._$grid.slideUp(300);
        },
        showGrid: function () {
            this._$grid.slideDown(300);
        },
        printGrid: function () {
            for (var row = 1; row < this._grid.length; row++) {
                var rowContent = '';
                for (var cell = 1; cell < this._grid[row].length; cell++) {
                    rowContent += ' - ' + this._grid[row][cell];
                }
                console.info(rowContent);
            }
        }
    });
});

// Вычитывание размера исходя из определённого элемента, можно указать или всегда использовать контейнер






function Items() {
}

Items.prototype = {
    items: []
};

function Elements() {

}

Elements.prototype = {
    _getElement: function (id) {

    },
    _getElements: function (selector) {
        return this.$table.find(selector);
    },
    _moveElement: function (x, y) {

    },
    _removeElement: function (id) {

    },
    /**
     * Удаление всех элементов таблицы
     * @private
     */
    _removeElements: function () {
    }
};

function Helpers() {

}

Helpers.prototype = {
    // calc...
    _getVal: function (value, defaultValue) {
        return (typeof value == 'undefined' ? defaultValue : value);
    },
    _logDebugInfo: function () {
        console.log(this._table);
    }
};

/*
 getCell: function () {

 },
 getCells: function () {

 },
 getCellsArea: function () {

 },
 _clearCell: function () {

 },
 _clearCells: function () {

 },
 _clearCellsArea: function () {
 // getCellsArea
 },
 _setCellsArea: function () {
 // getCellsArea
 },
 _addRow: function (side) {
 var rowIndex;
 var rowDefaultValue = [];
 var tableSize = this._getTableSize();
 var rowSize = tableSize.width;

 side = this._getVal(side, this.BOTTOM);

 if (side == this.TOP) {
 this.table.unshift(rowDefaultValue);
 rowIndex = 0;
 } else {
 this.table.push(rowDefaultValue);
 rowIndex = this.table.length - 1;
 }
 for (var i = 0; i < rowSize; i++) {
 this.__addCell(rowIndex);
 }

 return rowIndex;
 },
 _addCol: function (side) {
 // TODO
 },
 generateTable: function (size) {
 size = this._getVal(size, this._getTableSize());
 for (var i = 0; i < size.height; i++) {
 this._addRow();
 }
 },
 _getTableSize: function () {
 return this.options.size;
 },
 __addCell: function (row, side) {
 // WARNING Не учитывает заполненные ячейки
 var cellIndex;
 var cellDefaultValue = null;
 side = this._getVal(side, this.RIGHT);

 if (side == this.LEFT) {
 this.table[row].unshift(cellDefaultValue);
 cellIndex = 0;
 } else {
 this.table[row].push(cellDefaultValue);
 cellIndex = this.table[row].length - 1;
 }

 return cellIndex;
 },
 _setTable: function (table) {
 this.$table = (typeof table == 'object' ? table : $(table));
 return true;
 }
 */