<?php
/**
 * Created by Dmitriy Baibuhtin
 * Date: 13.02.2015
 */

namespace app\assets;
use yii\web\AssetBundle;
class AngularJsAsset extends AssetBundle
{
    public $sourcePath = '@app/vendor/bower';

    public $css = [
        'angular/angular-csp.css',
    ];
    public $js = [
        'angular/angular.min.js',
        'angular-resource/angular-resource.min.js',
    ];
}