<?php
namespace app\assets;

use yii\web\AssetBundle;

class InnerLayoutAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/inner.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}