/**
 *
 * @param properties array Свойства для заполнения
 * @param successCallback function Колбэк при успешном заполнении
 * @param timeoutCallback function Колбэк при таймауте
 * @param timeout number Таймаут (по умолчанию 30 секунд)
 * @constructor
 */
function DataCollection(properties, successCallback, timeoutCallback, timeout) {
    /** @var object Список свойств и статус обработки name: status . Статус заполнения устанавливается автоматически **/
    this._propertiesList = {};
    /** @var object Значения свойств */
    this._propertiesValues = {};
    /** @var object Идентификатор таймаута */
    this._timeoutId = null;
    this._successCallback = successCallback;
    /** @var bool Флаг окончания сбора данных. True при успешном сборе */
    this._dataCollected = false;
    /** @var bool Флаг окончания обработки собранных данных (true при любом исходе сбора) */
    this._dataProcessed = false;

    timeout = (angular.isDefined(timeout) ? timeout : 30);
    this.addProperties(properties);
    if (timeoutCallback && timeout) {
        this.enableTimeout(timeout, timeoutCallback);
    }
}

/**
 * Заполнение значения свойства
 * @param name
 * @param value
 */
DataCollection.prototype.setValue = function (name, value) {
    this._setValue(name, value);
    this._markFilled(name);
    this._processCollectedData();
};

/**
 * Заполнение значений свойств
 * @param values object {name: value}
 */
DataCollection.prototype.setValues = function (values) {
    angular.forEach(values, function (value, name) {
        this._setValue(name, value);
        this._markFilled(name);
    }, this);
    this._processCollectedData();
};

/**
 * Проверка существования значения свойства
 * @param name
 * @returns {boolean}
 */
DataCollection.prototype.valueExists = function (name) {
    return angular.isDefined(this._propertiesValues[name]);
};


/**
 * Проверка существования свойства
 * @param name
 * @returns {boolean}
 */
DataCollection.prototype.propertyExists = function (name) {
    return !angular.isUndefined(this._propertiesList[name]);
};

/**
 * Добавление свойств
 * @param properties
 */
DataCollection.prototype.addProperties = function (properties) {
    angular.forEach(properties, function (name, i) {
        this._propertiesList[name] = false;
    }, this);
};

/**
 * Проверка статуса сбора данных
 * @returns {boolean}
 */
DataCollection.prototype.dataCollected = function () {
    if (this._dataCollected == true) {
        return true;
    }

    var dataCollected = true;
    for (var key in this._propertiesList) if (this._propertiesList.hasOwnProperty(key)) {
        if (this._propertiesList[key] == false) {
            dataCollected = false;
            break;
        }
    }
    this._dataCollected = dataCollected;
    return dataCollected;
};

/**
 * Проверка статуса сбора и обработки данных (true при любом исходе сбора)
 * @returns {boolean|*}
 */
DataCollection.prototype.dataProcessed = function() {
    return this._dataProcessed;
};

/**
 * Получение данных собранных на данный момент
 * @returns {{}|*}
 */
DataCollection.prototype.getCollectedData = function () {
    return this._propertiesValues;
};

/**
 * Установка таймера по окончанию которого будет вызван колбэк провала. Предыдущий таймер будет сброшен
 * @param timeout
 * @param timeoutCallback
 */
DataCollection.prototype.enableTimeout = function (timeout, timeoutCallback) {
    if (!angular.isNumber(timeout) || !angular.isFunction(timeoutCallback)) {
        throw 'Не указан таймаут сбора данных или функция для обратного вызова';
    }

    if (this._timeoutId != null) {
        clearTimeout(this._timeoutId);
        this._timeoutId = null;
    }

    var context = this;
    this._timeoutId = setTimeout(function () {
        console.log('Сбор данных завершен по таймауту');
        timeoutCallback(context._propertiesValues);
        context._setDataProcessed(true);
    }, timeout * 1000);
};

/**
 * Обработка собранных данных. Вызов колбэка успеха
 */
DataCollection.prototype._processCollectedData = function () {
    if (this.dataCollected() == true) {
        this._successCallback(this.getCollectedData());
        this._setDataProcessed(true);
    }
};

/**
 * Помечает свойство как заполненное
 * @param key Название свойства
 * @private
 */
DataCollection.prototype._markFilled = function (key) {
    this._setProperty(key, true);
};

/**
 * Низкоуровневая установка статуса заполненности
 * @param key
 * @param value
 * @private
 */
DataCollection.prototype._setProperty = function (key, value) {
    if (!this.propertyExists(key)) {
        throw 'Свойство не существует';
    }
    this._propertiesList[key] = value;
};

/**
 * Низкоуровневая установка значения свойства
 * @param key
 * @param value
 * @private
 */
DataCollection.prototype._setValue = function (key, value) {
    if (!this.propertyExists(key)) {
        throw 'Свойство не существует';
    }

    this._propertiesValues[key] = value;
};

DataCollection.prototype._setDataProcessed = function (status) {
    this._dataProcessed = status;
};
