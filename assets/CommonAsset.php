<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

// FUTURE Логически разбить пакеты ресурсов для более удобного доступа и экономии трафика.
// Не забыть прописать регистрацию пакетов, там где они используются
class CommonAsset extends AssetBundle
{
    public $sourcePath = '@app/assets';

    public $js = [
        'common/js/functions.js',
        'common/js/scripts.js',
        'common/js/data-collection.js',
        'plugins/jquery.serialize-object/jquery.serialize-object.min.js',
        'plugins/jquery.ui.sortable-grid/jquery.ui.sortable-grid.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\JqueryUiAsset',
        'app\assets\AngularJsModuleAsset'
    ];
}