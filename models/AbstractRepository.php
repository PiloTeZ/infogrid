<?php
/**
 * TODO Удалить нахрен, бесполезный класс
 */
namespace app\models;

use yii\base\ErrorException;
use yii\base\Object;
use yii\web\ServerErrorHttpException;
use yii\db\ActiveRecord;

/**
 * Базовый класс-репозиторий для удобного доступа к БД, гибкого добавления новых возможностей
 * Создан для соблюдения принципов ООП в Yii2,
 * освобождения классов ActiveRecord от лишних функций (преобразование их в сущности)
 * @package app\modules\users\models\repositories
 */
abstract class AbstractRepository extends Object
{
    /** @var string Название класса сущности */
    public $entityClass;
    /** @var string Название класса конструктора запросов */
    public $entityActiveQueryClass;

    /**
     * @throws ErrorException
     */
    public function init()
    {
        parent::init();

        if (empty($this->entityClass) || empty($this->entityActiveQueryClass)) {
            throw new ErrorException('Ошибка конфигурации репозитория');
        }
    }

    /**
     *
     */
    public function beforeSave()
    {
        // :TODO Заменить на отслеживание запроса из вне
    }

    /**
     * Создание записи
     * @param ActiveRecord $entity
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Exception
     */
    public function create(ActiveRecord $entity)
    {
        if (!$entity->isNewRecord) {
            throw new ServerErrorHttpException('У записи уже есть идентификатор');
        }

        $this->beforeSave($entity);

        return $entity->insert();
    }

    /**
     * Обновление записи
     * @param ActiveRecord $entity
     * @return bool|int
     * @throws ServerErrorHttpException
     * @throws \Exception
     */
    public function update(ActiveRecord $entity)
    {
        if ($entity->isNewRecord) {
            throw new ServerErrorHttpException('Запись отсутствует');
        }
        $this->beforeSave($entity);

        return $entity->update();
    }

    /**
     * Если запись не существует, создание записи, иначе - обновление
     * @param ActiveRecord $entity
     * @return bool
     */
    public function save(ActiveRecord $entity)
    {
        $this->beforeSave($entity);

        return $entity->save();
    }

    /**
     * Удаление записи
     * @param ActiveRecord $entity
     * @return bool|int
     * @throws \Exception
     */
    public function delete(ActiveRecord $entity)
    {
        return $entity->delete();
    }


    /**
     * Массовое удаление через конструктор запросов
     * @param ActiveQuery $activeQuery
     */
    /*
    public function deleteMultiple(ActiveQuery $activeQuery)
    {
        // return $entityClass::deleteAll($entitiesQuery->where);
    }
    */
}