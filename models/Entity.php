<?php
namespace app\models;

use app\components\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\ServerErrorHttpException;

class Entity extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        return \Yii::createObject(ActiveQuery::className(), [get_called_class()]);
    }

    public function scenarios()
    {
        // Удаление стандартных сценариев. В каждой сущности обязательно должны быть свои сценарии
        throw new ServerErrorHttpException('Не определены сценарии модели');
    }
}