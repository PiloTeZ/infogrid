<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'ru-RU',
    'bootstrap' => ['log'],
    'modules' => [
        'users' => [
            'class' => 'app\modules\users\Users',
        ],
        'gadgets' => [
            'class' => 'app\modules\gadgets\Gadgets',
        ],
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
        ],
        'assetManager' => [
            // TODO Убрать на продакшене
            'forceCopy' => true,
        ],
        'flashMessages' => [
            'class' => 'app\components\FlashMessages',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => '4757569',
                    'clientSecret' => '6DcVkpZJ3fBuqM3I49qD',
                    'title' => 'ВКонтакте',
                    'scope' => 'offline',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '371560346336487',
                    'clientSecret' => '768025fe12b046203ef5f8cdf3ca056a',
                    'title' => 'Facebook',
                    'scope' => 'offline_access,email,manage_notifications,read_mailbox',
                ],
                'odnoklassniki' => [
                    'class' => 'app\components\authClients\Odnoklassniki',
                    'clientId' => '1124036608',
                    'clientPublic' => 'CBABHQPDEBABABABA',
                    'clientSecret' => 'E9FA9B58F212DA1886CAE23D',
                    'title' => 'Одноклассники',
                    'scope' => 'VALUABLE_ACCESS',
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                    'clientId' => '801059997007-jkhvjk2q3inre1v2jku1ghjvtcjd5lvg.apps.googleusercontent.com',
                    'clientSecret' => 'oDI61LR1nincLVthuqLmQ7Eb',
                    'title' => 'Google',
                ],
                'yandex' => [
                    'class' => 'yii\authclient\clients\YandexOAuth',
                    'clientId' => '74e4817007624e9995daaa57c6b1dc22',
                    'clientSecret' => 'be5ec597cbb1444596e10d8d54053ee1',
                    'title' => 'Яндекс',
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    'consumerKey' => 'AQ8KCtriyPdl2SDdX6naNuCXt',
                    'consumerSecret' => 'CNGwwAJEsE5nKpf5NuRxiKtui2cNqjjRLzVLxU0o2MUpUcCGKk',
                    'title' => 'Twitter',
                ],
            ],
        ],
        'request' => [
            'cookieValidationKey' => '9gD3VAnONxY5v6as2XNlhpPpwJve4eE0',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\users\models\entities\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    // QInfo widget generator
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'widget' => [
                'class' => 'app\components\gii\generators\widget\Generator',
                'templates' => ['default' => '@app/components/gii/generators/widget/default']
            ],
        ]
    ];
    $config['modules']['gii'];
}

return $config;