<?php


namespace app\controllers;

use app\modules\users\components\ProvidersManager;
use Yii;
use yii\web\Controller;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\Response;

class ApiProvidersProxyController extends Controller
{
    public function actionApi()
    {
        if (Yii::$app->user->isGuest) {
            throw new UnauthorizedHttpException('Требуется авторизация');
        }
        $request = Yii::$app->request;
        $providerAccountId = (int) $request->post('providerAccountId');
        $apiSubUrl = (string) $request->post('apiSubUrl');
        $params = (array) $request->post('params', []);
        $method = (string) $request->post('method', 'GET');
        $headers = (array) $request->post('headers', []);

        $providersManager = new ProvidersManager;
        $account = Yii::$app->user->identity->getProviderAccount($providerAccountId)->one();
        if (empty($account)) {
            throw new ServerErrorHttpException('Аккаунт не найден');
        }
        /** @var \yii\authclient\BaseOAuth $provider */
        $provider = $providersManager->getProviderManagerByAccount($account);
        if (empty($account)) {
            throw new ServerErrorHttpException('API провайдер для данного сервиса не найден');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $provider->api($apiSubUrl, $method, $params, $headers);
    }
}