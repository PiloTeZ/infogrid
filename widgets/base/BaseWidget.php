<?php

namespace app\widgets\base;

use app\components\WidgetHelper;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\ServerErrorHttpException;

// TODO Сделать общее свойство обозначающее использование JS любого типа, например useJsLibs и заменить длинные условия типа
// if ($this->widgetUseJs || $this->widgetUseAngularJsComponents || $this->widgetUseAngularJsController) на
// if ($this->useJsLibs)
// Найти все места использования

abstract class BaseWidget extends Widget
{
	/** @var WidgetHelper */
	public $widgetHelper;

	/** @var bool Подключить ресурсы js/functions.js */
	protected $widgetUseJs = false;
	/** @var bool Подключить ресурс js/angular.js */
	protected $widgetUseAngularJsComponents = false;
	/**
	 * @var bool Обернуть контент виджета в контроллер angularJs
	 * Название контроллера будет иметь вид WidgetNameController
	 * */
	protected $widgetUseAngularJsController = false;
	/** @var bool Подключить ресурс css/styles.css */
	protected $widgetUseStyles = false;

	public function init()
	{
		parent::init();
		$this->widgetHelper = new WidgetHelper($this);

		$this->widgetPrepareConfig();
		if ($this->widgetUseJs) {
			$this->widgetPrepareJs();
		}
		if ($this->widgetUseAngularJsComponents) {
			$this->widgetPrepareAngularJsComponents();
		}
		if ($this->widgetUseStyles) {
			$this->widgetPrepareStyles();
		}
	}

	/**
	 * Подготовка конфигурации виджета
	 */
	protected function widgetPrepareConfig()
	{
		/* Контроллер описывается вместе с остальными компонентами AngularJs,
		поэтому использование компонентов AngularJs включается принудительно */
		if ($this->widgetUseAngularJsController) {
			$this->widgetUseAngularJsComponents = true;
		}
	}

	/**
	 * Подключение скриптов JS
	 */
	protected function widgetPrepareJs()
	{
		$jsFunctionsAsset = Yii::$app->assetManager->publish($this->widgetHelper->getAssetsPath() . 'js/functions.js');
		$this->view->registerJsFile($jsFunctionsAsset[1], ['depends' => [
			'\yii\web\JqueryAsset',
			'app\assets\JqueryUiAsset',
		]]);
	}

	/**
	 * Подготовка параметров для представления
	 * @param $params
	 * @throws ServerErrorHttpException
	 */
	protected function widgetPrepareViewParams(&$params)
	{
		// Проверка конфликта параметров
		if (!empty($params['widgetId']) || !empty($params['jsParamsJson'])) {
			throw new ServerErrorHttpException('Конфликт параметров виджета');
		}

		// Подготовка параметров для JS (кодирование в JSON)
		if ($this->widgetUseJs || $this->widgetUseAngularJsComponents || $this->widgetUseAngularJsController) {
			if (empty($params['jsParams'])) {
				$params['jsParams'] = [];
				$params['jsParamsJson'] = '{}';
			}
			else {
				$params['jsParamsJson'] = Json::encode($params['jsParams']);
			}
		}

		// Добавление идентификатора виджета
		$params['widgetId'] = $this->widgetGetId();
	}

	/**
	 * Подключение скриптов angular js
	 */
	protected function widgetPrepareAngularJsComponents()
	{
		$angularJsAsset = Yii::$app->assetManager->publish($this->widgetHelper->getAssetsPath() . 'js/angular.js');
		$this->view->registerJsFile($angularJsAsset[1], ['depends' => ['app\assets\AngularJsModuleAsset']]);
	}

	/**
	 * Оборачивание контента виджета в контроллер AngularJs
	 * @param $widgetContent
	 */
	protected function widgetWrapByAngularJsController(&$widgetContent, &$params)
	{
		$widgetContent = Html::tag('div', $widgetContent, [
			'ng-controller' => $this->widgetGetAngularJsControllerName(),
			'ng-init' => 'init(' . $params['jsParamsJson'] . ')',
		]);
	}

	/**
	 * Название контроллера angularJs, которым будет обёрнут виджет
	 * @return string
	 */
	protected function widgetGetAngularJsControllerName()
	{
		return $this->widgetHelper->getFullName() . 'Controller';
	}

	/**
	 * Подключение файлов стилей
	 */
	protected function widgetPrepareStyles()
	{
		$stylesAsset = Yii::$app->assetManager->publish($this->widgetHelper->getAssetsPath() . 'css/styles.css');
		$this->view->registerJsFile($stylesAsset[1]);
	}

	/**
	 * Получение уникального идентификатора виджета.
	 * Используется в DOM
	 * @return string
	 */
	protected function widgetGetId()
	{
		return $this->id;
	}

	/**
	 * Событие перед генерацией представления виджета
	 * @param $view
	 * @param $params
	 * @param $widgetContent
	 */
	protected function beforeRender(&$view, &$params, &$widgetContent)
	{
		// Дополнительно передаются jsParamsJson сгенерированные из jsParams переданные из виджета
		$this->widgetPrepareViewParams($params);
	}

	/**
	 * Событие после генерации представления виджета
	 * @param $view
	 * @param $params
	 * @param $widgetContent
	 */
	protected function afterRender(&$view, &$params, &$widgetContent)
	{
		if ($this->widgetUseAngularJsController) {
			$this->widgetWrapByAngularJsController($widgetContent, $params);
		}
	}

	/**
	 * Добавление новых методов beforeRender и afterRender
	 * @param string $view
	 * @param array $params
	 * @return string
	 */
	public function render($view, $params = [])
	{
		$widgetContent = '';
		$this->beforeRender($view, $params, $widgetContent);
		$widgetContent .= parent::render($view, $params);
		$this->afterRender($view, $params, $widgetContent);

		return $widgetContent;
	}
}